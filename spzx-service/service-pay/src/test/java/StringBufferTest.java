/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/29 17:50
 */
public class StringBufferTest {
    public static void main(String[] args) {
        System.out.println(test1());
    }

    public static String test1() {
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < 10; i++) {
            stringBuffer.append(i);
        }
        return stringBuffer.toString();
    }
}
