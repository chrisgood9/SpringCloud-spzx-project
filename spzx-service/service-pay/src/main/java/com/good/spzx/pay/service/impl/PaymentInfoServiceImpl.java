package com.good.spzx.pay.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSON;
import com.good.spzx.feign.order.OrderFeignClient;
import com.good.spzx.feign.product.ProductFeignClient;
import com.good.spzx.model.dto.product.SkuSaleDto;
import com.good.spzx.model.entity.order.OrderInfo;
import com.good.spzx.model.entity.order.OrderItem;
import com.good.spzx.model.entity.pay.PaymentInfo;
import com.good.spzx.pay.mapper.PaymentInfoMapper;
import com.good.spzx.pay.service.PaymentInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/29 17:19
 */
@Service
public class PaymentInfoServiceImpl implements PaymentInfoService {

    @Autowired
    private PaymentInfoMapper paymentInfoMapper;

    @Autowired
    private OrderFeignClient orderFeignClient;

    @Autowired
    private ProductFeignClient productFeignClient;

    /**
     * 查询支付信息数据，
     * 如果已经已经存在了就不用进行保存(一个订单支付失败以后可以继续支付)
     *
     * @param orderNo
     * @return
     */
    @Override
    public PaymentInfo savePaymentInfo(String orderNo) {
        // 根据订单编号orderNo - 获取支付信息
        PaymentInfo paymentInfo = paymentInfoMapper.getPaymentByOrderNo(orderNo);
        if (ObjectUtil.isEmpty(paymentInfo)) {
            // 如果没有支付信息，则添加支付信息
            OrderInfo orderInfo = orderFeignClient.getOrderInfoByOrderNo(orderNo).getData();
            paymentInfo = new PaymentInfo();
            paymentInfo.setUserId(orderInfo.getUserId());
            paymentInfo.setPayType(orderInfo.getPayType());

            StringBuffer stringBuffer = new StringBuffer();
            for (OrderItem orderItem : orderInfo.getOrderItemList()) {
                stringBuffer.append(orderItem.getSkuName()).append(" ");
            }
            paymentInfo.setContent(stringBuffer.toString());
            paymentInfo.setAmount(orderInfo.getTotalAmount());
            paymentInfo.setOrderNo(orderNo);
            paymentInfo.setPaymentStatus(0); // 支付状态：未支付
            // 添加
            paymentInfoMapper.save(paymentInfo);
        }
        return paymentInfo;
    }

    @Override
    public void updatePaymentStatus(Map<String, String> paramMap) {
        // 从map中获取订单id(orderId) -> 查询出PaymentInfo
        PaymentInfo paymentInfo = paymentInfoMapper.getPaymentByOrderNo(paramMap.get("out_trade_no"));
        if (paymentInfo.getPaymentStatus() == 1) {
            return;
        }
        //更新支付信息
        paymentInfo.setPaymentStatus(1);
        paymentInfo.setOutTradeNo(paramMap.get("trade_no"));
        paymentInfo.setCallbackTime(new Date());
        paymentInfo.setCallbackContent(JSON.toJSONString(paramMap)); // 回调信息
        // 更新支付数据
        paymentInfoMapper.updatePaymentInfoById(paymentInfo);
        // 更新订单状态
        orderFeignClient.updateOrderStatus(paymentInfo.getOrderNo());
        // 更新sku商品销量
        OrderInfo orderInfo = orderFeignClient.getOrderInfoByOrderNo(paymentInfo.getOrderNo()).getData();
        List<SkuSaleDto> skuSaleDtoList = orderInfo.getOrderItemList().stream().map(orderItem -> {
            SkuSaleDto skuSaleDto = new SkuSaleDto();
            skuSaleDto.setSkuId(orderItem.getSkuId());
            skuSaleDto.setNum(orderItem.getSkuNum());
            return skuSaleDto;
        }).collect(Collectors.toList());
        productFeignClient.updateSkuSaleNum(skuSaleDtoList);
    }
}
