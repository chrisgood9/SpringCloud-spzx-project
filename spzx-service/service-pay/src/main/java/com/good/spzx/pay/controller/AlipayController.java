package com.good.spzx.pay.controller;

import com.alipay.api.AlipayApiException;
import com.alipay.api.internal.util.AlipaySignature;
import com.good.spzx.model.vo.common.Result;
import com.good.spzx.model.vo.common.ResultCodeEnum;
import com.good.spzx.pay.properties.AlipayProperties;
import com.good.spzx.pay.service.AlipayService;
import com.good.spzx.pay.service.PaymentInfoService;
import io.swagger.v3.oas.annotations.Parameter;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/30 13:28
 */
@Controller
@RequestMapping("/api/order/alipay")
public class AlipayController {

    @Autowired
    private AlipayService alipayService;

    @Autowired
    private AlipayProperties alipayProperties;

    @Autowired
    private PaymentInfoService paymentInfoService;

    /**
     * 支付宝下单
     *
     * @param orderNo 订单号
     * @return 支付表单form
     */
    @GetMapping("submitAlipay/{orderNo}")
    @ResponseBody
    public Result<String> submitAlipay(@PathVariable(value = "orderNo") String orderNo) {
        String form = alipayService.submitAlipay(orderNo);
        return Result.build(form, ResultCodeEnum.SUCCESS);
    }

    /**
     * 支付宝异步回调")
     *
     * @param paramMap 由支付宝接口返回的数据
     * @param request
     * @return
     */
    @RequestMapping("callback/notify")
    @ResponseBody
    public String alipayNotify(@RequestParam Map<String, String> paramMap,
                               HttpServletRequest request) {
        // 非对称加密
        boolean signVerified = false; //调用SDK验证签名
        try {
            signVerified = AlipaySignature.rsaCheckV1(paramMap, alipayProperties.getAlipayPublicKey(), AlipayProperties.charset, AlipayProperties.sign_type);
        } catch (AlipayApiException e) {
            e.printStackTrace();
        }
        // 交易状态 - 由支付宝接口返回的数据中获取
        String trade_status = paramMap.get("trade_status");

        if (signVerified) {
            // 校验成功 -> 更新支付状态
            paymentInfoService.updatePaymentStatus(paramMap);
        } else {
            return "failure";
        }
        return "failure";
    }
}
