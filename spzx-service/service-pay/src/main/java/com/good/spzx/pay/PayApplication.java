package com.good.spzx.pay;

import com.good.spzx.annotation.EnableUserLoginAuthInterceptor;
import com.good.spzx.pay.properties.AlipayProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/29 17:11
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients(basePackages = {"com.good.spzx"})
@EnableUserLoginAuthInterceptor
@EnableConfigurationProperties(value = {AlipayProperties.class})
public class PayApplication {
    public static void main(String[] args) {
        SpringApplication.run(PayApplication.class, args);
    }
}
