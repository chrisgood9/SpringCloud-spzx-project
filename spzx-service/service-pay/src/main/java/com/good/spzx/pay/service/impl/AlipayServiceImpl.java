package com.good.spzx.pay.service.impl;

import com.alibaba.fastjson.JSON;
import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.request.AlipayTradeWapPayRequest;
import com.alipay.api.response.AlipayTradeWapPayResponse;
import com.good.spzx.exception.MyException;
import com.good.spzx.model.entity.pay.PaymentInfo;
import com.good.spzx.model.vo.common.ResultCodeEnum;
import com.good.spzx.pay.mapper.PaymentInfoMapper;
import com.good.spzx.pay.properties.AlipayProperties;
import com.good.spzx.pay.service.AlipayService;
import com.good.spzx.pay.service.PaymentInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashMap;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/30 13:29
 */
@Service
public class AlipayServiceImpl implements AlipayService {

    @Autowired
    private PaymentInfoService paymentInfoService;

    @Autowired
    private AlipayProperties alipayProperties;

    @Autowired
    private AlipayClient alipayClient;

    /**
     * 支付宝支付
     *
     * @param orderNo
     * @return
     */
    @Override
    public String submitAlipay(String orderNo) {
        // 1. 保存支付记录
        PaymentInfo paymentInfo = paymentInfoService.savePaymentInfo(orderNo);
        // 2. 调用支付宝服务接口
        // 构建需要的参数，进行调用
        AlipayTradeWapPayRequest alipayTradeWapPayRequest = new AlipayTradeWapPayRequest();
        // 同步回调
        alipayTradeWapPayRequest.setReturnUrl(alipayProperties.getReturnPaymentUrl());
        // 异步回调 - 支付成功后更新信息 - 具体在application-alipay.yml
        alipayTradeWapPayRequest.setNotifyUrl(alipayProperties.getNotifyPaymentUrl());

        // 准备请求参数，声明一个map集合
        HashMap<String, Object> map = new HashMap<>();
        map.put("out_trade_no", paymentInfo.getOrderNo());
        map.put("product_code", "QUICK_WAP_WAY");
//        map.put("total_amount", paymentInfo.getAmount());
        // 这里设置 支付金额固定为0.01
        map.put("total_amount", new BigDecimal("0.01"));
        map.put("subject", paymentInfo.getContent());

        alipayTradeWapPayRequest.setBizContent(JSON.toJSONString(map));
        //调用支付宝服务接口
        try {
            AlipayTradeWapPayResponse response = alipayClient.pageExecute(alipayTradeWapPayRequest);
            if (response.isSuccess()) {
                // 支付成功 -> 返回支付表单
                return response.getBody();
            } else {
                // 支付失败 -> 返回异常
                throw new MyException(ResultCodeEnum.DATA_ERROR);
            }
        } catch (AlipayApiException e) {
            throw new RuntimeException();
        }
    }
}
