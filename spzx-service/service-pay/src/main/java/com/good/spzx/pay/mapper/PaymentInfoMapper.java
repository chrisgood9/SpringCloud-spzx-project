package com.good.spzx.pay.mapper;

import com.good.spzx.model.entity.pay.PaymentInfo;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/29 17:20
 */
@Mapper
public interface PaymentInfoMapper {

    // 根据订单编号orderNo - 获取支付信息
    PaymentInfo getPaymentByOrderNo(String orderNo);

    // 查询支付信息数据
    void save(PaymentInfo paymentInfo);

    // 根据id更新支付信息
    void updatePaymentInfoById(PaymentInfo paymentInfo);
}