package com.good.spzx.pay.service;

import com.good.spzx.model.entity.pay.PaymentInfo;

import java.util.Map;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/29 17:19
 */
public interface PaymentInfoService {

    // 保存支付信息
    PaymentInfo savePaymentInfo(String orderNo);

    // 支付成功 - 更新支付状态
    void updatePaymentStatus(Map<String, String> paramMap);
}
