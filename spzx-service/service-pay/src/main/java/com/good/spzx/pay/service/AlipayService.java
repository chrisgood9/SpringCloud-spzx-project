package com.good.spzx.pay.service;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/30 13:29
 */
public interface AlipayService {

    // 支付宝下单 -> 获取支付表单form
    String submitAlipay(String orderNo);

}
