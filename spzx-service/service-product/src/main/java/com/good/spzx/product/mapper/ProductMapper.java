package com.good.spzx.product.mapper;

import com.good.spzx.model.entity.product.Product;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/26 20:31
 */
@Mapper
public interface ProductMapper {

    // 根据id获取商品信息
    Product getProductById(Long productId);

}
