package com.good.spzx.product.mapper;

import com.good.spzx.model.entity.product.ProductDetails;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/26 20:44
 */
@Mapper
public interface ProductDetailsMapper {
    // 根据商品id获取商品详情信息
    ProductDetails getDetailsByProductId(Long productId);
}
