package com.good.spzx.product.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.good.spzx.model.dto.h5.ProductSkuDto;
import com.good.spzx.model.entity.product.ProductSku;
import com.good.spzx.model.vo.h5.ProductItemVo;
import com.good.spzx.product.mapper.ProductSkuMapper;
import com.good.spzx.product.service.ProductSkuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/25 16:02
 */
@Service
public class ProductSkuServiceImpl implements ProductSkuService {

    @Autowired
    private ProductSkuMapper productSkuMapper;

    @Override
    public List<ProductSku> selectProductSkuBySale() {
        return productSkuMapper.selectProductSkuBySale();
    }

    @Override
    public PageInfo<ProductSku> findByPage(Integer page, Integer limit, ProductSkuDto productSkuDto) {
        PageHelper.startPage(page, limit);
        List<ProductSku> productSkuList = productSkuMapper.findByPage(productSkuDto);
        return new PageInfo<>(productSkuList);
    }

    @Override
    public ProductSku getProductBySkuId(Long skuId) {
        return productSkuMapper.getProductBySkuId(skuId);
    }

}
