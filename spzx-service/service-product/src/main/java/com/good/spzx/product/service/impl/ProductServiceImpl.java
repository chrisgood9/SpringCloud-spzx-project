package com.good.spzx.product.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.alibaba.fastjson.JSON;
import com.good.spzx.model.dto.product.SkuSaleDto;
import com.good.spzx.model.entity.product.Product;
import com.good.spzx.model.entity.product.ProductDetails;
import com.good.spzx.model.entity.product.ProductSku;
import com.good.spzx.model.vo.h5.ProductItemVo;
import com.good.spzx.product.mapper.ProductDetailsMapper;
import com.good.spzx.product.mapper.ProductMapper;
import com.good.spzx.product.mapper.ProductSkuMapper;
import com.good.spzx.product.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/26 20:30
 */
@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductSkuMapper productSkuMapper;

    @Autowired
    private ProductMapper productMapper;

    @Autowired
    private ProductDetailsMapper productDetailsMapper;

    @Override
    public ProductItemVo getProductItem(Long skuId) {
        // 1. 获取商品sku信息 -> productId
        ProductSku productSku = productSkuMapper.getSkuById(skuId);
        // 2. 根据productId获取product信息
        Long productId = productSku.getProductId();
        Product product = productMapper.getProductById(productId);
        // 3. 获取当前productId下的所有sku信息 (同一个商品下面的sku信息列表)
        List<ProductSku> productSkuList = productSkuMapper.getSkuListByProductId(productId);
        // 4. 建立sku规格(skuSpec)与skuId的关系 ->
        Map<String, Object> skuSpecValueMap = new HashMap<>();
        productSkuList.forEach(item -> {
            skuSpecValueMap.put(item.getSkuSpec(), item.getId());
        });
        // 5. 根据商品id获取商品详情信息
        ProductDetails productDetails = productDetailsMapper.getDetailsByProductId(productId);

        // 封装数据
        ProductItemVo productItemVo = new ProductItemVo();
        productItemVo.setProduct(product);
        productItemVo.setProductSku(productSku);
        productItemVo.setSpecValueList(JSON.parseArray((product.getSpecValue())));
        productItemVo.setSliderUrlList(Arrays.asList(product.getSliderUrls().split(",")));
        productItemVo.setDetailsImageUrlList(Arrays.asList(productDetails.getImageUrls().split(",")));
        productItemVo.setSkuSpecValueMap(skuSpecValueMap);
        return productItemVo;
    }

    @Override
    public Boolean updateSkuSaleNum(List<SkuSaleDto> skuSaleDtoList) {
        if (!CollectionUtil.isEmpty(skuSaleDtoList)) {
            for (SkuSaleDto saleDto : skuSaleDtoList) {
                productSkuMapper.updateSale(saleDto.getSkuId(), saleDto.getNum());
            }
        }
        return true;
    }
}
