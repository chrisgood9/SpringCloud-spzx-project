package com.good.spzx.product.service.impl;

import com.good.spzx.model.entity.product.Brand;
import com.good.spzx.product.mapper.BrandMapper;
import com.good.spzx.product.service.BrandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/26 13:43
 */
@Service
public class BrandServiceImpl implements BrandService {

    @Autowired
    private BrandMapper brandMapper;

    @Override
    public List<Brand> findAllBrand() {
        return brandMapper.findAllBrand();
    }
}
