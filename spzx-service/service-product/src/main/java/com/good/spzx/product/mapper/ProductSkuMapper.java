package com.good.spzx.product.mapper;

import com.good.spzx.model.dto.h5.ProductSkuDto;
import com.good.spzx.model.entity.product.ProductSku;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/25 16:03
 */
@Mapper
public interface ProductSkuMapper {

    List<ProductSku> selectProductSkuBySale();

    List<ProductSku> findByPage(ProductSkuDto productSkuDto);

    // 根据id获取sku信息
    ProductSku getSkuById(Long skuId);

    // 获取当前productId下的所有sku信息 (同一个商品下面的sku信息列表)
    List<ProductSku> getSkuListByProductId(Long productId);

    // 根据skuId获取商品Sku信息
    ProductSku getProductBySkuId(Long skuId);

    // 更新销量 - (销量增加,库存量减少)
    void updateSale(@Param("skuId") Long skuId, @Param("num") Integer num);
}
