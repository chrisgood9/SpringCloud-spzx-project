package com.good.spzx.product.mapper;

import com.good.spzx.model.entity.product.Category;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/25 16:03
 */
@Mapper
public interface CategoryMapper {

    // 查询一级分类数据
    List<Category> selectOneCategory();

    // 查询所有分类数据
    List<Category> allCategoryList();

}
