package com.good.spzx.product.mapper;

import com.good.spzx.model.entity.product.Brand;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/26 13:45
 */
@Mapper
public interface BrandMapper {

    List<Brand> findAllBrand();

}
