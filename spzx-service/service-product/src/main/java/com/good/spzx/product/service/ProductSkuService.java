package com.good.spzx.product.service;

import com.github.pagehelper.PageInfo;
import com.good.spzx.model.dto.h5.ProductSkuDto;
import com.good.spzx.model.entity.product.ProductSku;
import com.good.spzx.model.vo.h5.ProductItemVo;

import java.util.List;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/25 16:01
 */
public interface ProductSkuService {

    // 畅销商品-前十条记录
    List<ProductSku> selectProductSkuBySale();

    // 条件分页查询 - 所有商品列表
    PageInfo<ProductSku> findByPage(Integer page, Integer limit, ProductSkuDto productSkuDto);

    // 获取商品sku信息
    ProductSku getProductBySkuId(Long skuId);
}
