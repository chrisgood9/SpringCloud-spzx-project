package com.good.spzx.product.service.impl;

import com.alibaba.fastjson.JSON;
import com.good.spzx.model.entity.product.Category;
import com.good.spzx.product.mapper.CategoryMapper;
import com.good.spzx.product.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/25 16:02
 */
@Service
public class CategoryServiceImpl implements CategoryService {

    public static final String CATEGORY_ONE = "category:one";

    @Autowired
    private CategoryMapper categoryMapper;

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @Override
    public List<Category> selectOneCategory() {
        // 缓存中存在 -> 缓存中获取
        String categoryOneJson = redisTemplate.opsForValue().get(CATEGORY_ONE);
        if (StringUtils.hasText(categoryOneJson)) {
            List<Category> exitRedisCategoryList = JSON.parseArray(categoryOneJson, Category.class);
            return exitRedisCategoryList;
        }
        // 缓存中不存在 -> 数据中获取，并加入到缓存中
        List<Category> categoryList = categoryMapper.selectOneCategory();
        redisTemplate.opsForValue().set(CATEGORY_ONE,
                JSON.toJSONString(categoryList),
                7, TimeUnit.DAYS);
        return categoryList;
    }

    @Override
    public List<Category> findCategoryTree() {
        // 查询所有分类，返回list集合
        List<Category> allCategoryList = categoryMapper.allCategoryList();

        // 遍历所有分类list集合，parentId = 0 => 获取一级分类
        List<Category> oneCategoryList = allCategoryList.stream()
                .filter(category -> category.getParentId().longValue() == 0)
                .collect(Collectors.toList());

        // 遍历一级分类，根据 id = parentId => 获取二级分类
        oneCategoryList.forEach(oneCategory -> {
            List<Category> twoCategoryList = allCategoryList.stream()
                    .filter(category -> category.getParentId() == oneCategory.getId())
                    .collect(Collectors.toList());
            // 封装二级分类
            oneCategory.setChildren(twoCategoryList);

            // 遍历二级分类，根据 id = parentId => 获取三级级分类
            twoCategoryList.forEach(twoCategory -> {
                List<Category> threeCategoryList = allCategoryList.stream()
                        .filter(category -> category.getParentId() == twoCategory.getId())
                        .collect(Collectors.toList());
                // 封装三级分类
                twoCategory.setChildren(threeCategoryList);
            });
        });

        return oneCategoryList;
    }
}
