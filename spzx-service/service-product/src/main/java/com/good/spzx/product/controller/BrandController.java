package com.good.spzx.product.controller;

import com.good.spzx.model.entity.product.Brand;
import com.good.spzx.model.vo.common.Result;
import com.good.spzx.model.vo.common.ResultCodeEnum;
import com.good.spzx.product.service.BrandService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/26 13:42
 */
@Tag(name = "品牌管理")
@RestController
@RequestMapping(value = "/api/product/brand")
public class BrandController {

    @Autowired
    private BrandService brandService;

    /**
     * 查询所有品牌信息
     *
     * @return
     */
    @GetMapping("/findAll")
    public Result findAllBrand() {
        List<Brand> brandList = brandService.findAllBrand();
        return Result.build(brandList, ResultCodeEnum.SUCCESS);
    }

}
