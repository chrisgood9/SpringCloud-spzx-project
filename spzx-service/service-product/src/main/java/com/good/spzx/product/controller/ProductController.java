package com.good.spzx.product.controller;

import com.github.pagehelper.PageInfo;
import com.good.spzx.model.dto.h5.ProductSkuDto;
import com.good.spzx.model.dto.product.SkuSaleDto;
import com.good.spzx.model.entity.product.ProductSku;
import com.good.spzx.model.vo.common.Result;
import com.good.spzx.model.vo.common.ResultCodeEnum;
import com.good.spzx.model.vo.h5.ProductItemVo;
import com.good.spzx.product.service.ProductService;
import com.good.spzx.product.service.ProductSkuService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/26 14:01
 */
@Tag(name = "商品列表管理")
@RestController
@RequestMapping(value = "/api/product")
public class ProductController {

    @Autowired
    private ProductSkuService productSkuService;

    @Autowired
    private ProductService productService;

    /**
     * 条件分页查询 - 所有商品列表
     *
     * @param page
     * @param limit
     * @param productSkuDto
     * @return
     */
    @GetMapping(value = "/{page}/{limit}")
    public Result<PageInfo<ProductSku>> findByPage(@PathVariable("page") Integer page,
                                                   @PathVariable("limit") Integer limit,
                                                   ProductSkuDto productSkuDto) {
        PageInfo<ProductSku> pageInfo = productSkuService.findByPage(page, limit, productSkuDto);
        return Result.build(pageInfo, ResultCodeEnum.SUCCESS);
    }

    /**
     * 商品的详情数据
     * 1、商品的基本信息
     * 2、当前商品sku的基本信息
     * 3、商品轮播图信息
     * 4、商品详情（详细为图片列表）
     * 5、商品规格信息
     * 6、当前商品sku的规格属性
     *
     * @param skuId
     * @return
     */
    @GetMapping("item/{skuId}")
    public Result getProductItem(@PathVariable("skuId") Long skuId) {
        ProductItemVo productItemVo = productService.getProductItem(skuId);
        return Result.build(productItemVo, ResultCodeEnum.SUCCESS);
    }

    /**
     * 获取商品sku信息
     *
     * @param skuId
     * @return
     */
    @GetMapping("/getProductBySkuId/{skuId}")
    public Result getProductBySkuId(@PathVariable("skuId") Long skuId) {
        ProductSku productSku = productSkuService.getProductBySkuId(skuId);
        return Result.build(productSku, ResultCodeEnum.SUCCESS);
    }

    /**
     * 更新商品sku销量
     *
     * @param skuSaleDtoList
     * @return
     */
    @PostMapping("updateSkuSaleNum")
    public Boolean updateSkuSaleNum(@RequestBody List<SkuSaleDto> skuSaleDtoList) {
        return productService.updateSkuSaleNum(skuSaleDtoList);
    }
}
