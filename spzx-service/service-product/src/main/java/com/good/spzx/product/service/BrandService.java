package com.good.spzx.product.service;

import com.good.spzx.model.entity.product.Brand;

import java.util.List;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/26 13:43
 */
public interface BrandService {
    
    // 查询所有品牌信息
    List<Brand> findAllBrand();

}
