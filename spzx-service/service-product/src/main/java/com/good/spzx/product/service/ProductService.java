package com.good.spzx.product.service;

import com.good.spzx.model.dto.product.SkuSaleDto;
import com.good.spzx.model.vo.h5.ProductItemVo;

import java.util.List;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/26 20:26
 */
public interface ProductService {

    // 获取商品详情信息
    ProductItemVo getProductItem(Long skuId);

    // 更新商品sku销量
    Boolean updateSkuSaleNum(List<SkuSaleDto> skuSaleDtoList);
}
