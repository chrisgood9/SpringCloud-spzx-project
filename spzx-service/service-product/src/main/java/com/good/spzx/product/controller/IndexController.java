package com.good.spzx.product.controller;


import com.good.spzx.model.entity.product.Category;
import com.good.spzx.model.entity.product.ProductSku;
import com.good.spzx.model.vo.common.Result;
import com.good.spzx.model.vo.common.ResultCodeEnum;
import com.good.spzx.model.vo.h5.IndexVo;
import com.good.spzx.product.service.CategoryService;
import com.good.spzx.product.service.ProductSkuService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/25 16:02
 */
@Tag(name = "首页接口管理")
@RestController
@RequestMapping(value = "/api/product/index")
public class IndexController {

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private ProductSkuService productService;

    /**
     * 获取首页数据
     *
     * @return
     */
    @GetMapping
    public Result indexData() {
        // 所有一级分类信息
        List<Category> categoryList = categoryService.selectOneCategory();
        // 畅销商品-前十条记录
        List<ProductSku> productSkuList = productService.selectProductSkuBySale();
        // 封装数据
        IndexVo indexVo = new IndexVo();
        indexVo.setCategoryList(categoryList);
        indexVo.setProductSkuList(productSkuList);
        return Result.build(indexVo, ResultCodeEnum.SUCCESS);
    }


}
