package com.good.spzx.product.service;

import com.good.spzx.model.entity.product.Category;

import java.util.List;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/25 16:01
 */
public interface CategoryService {

    // 所有一级分类信息
    List<Category> selectOneCategory();

    // 查询所有分类信息 (一级分类、二级分类、三级分类)
    List<Category> findCategoryTree();
}
