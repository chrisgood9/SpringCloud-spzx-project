package com.good.spzx.order.mapper;

import com.good.spzx.model.entity.order.OrderInfo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/28 14:14
 */
@Mapper
public interface OrderInfoMapper {

    void save(OrderInfo orderInfo);

    OrderInfo getOrderInfoById(Long orderId);

    // 根据用户id和订单状态 -> 获取用户订单信息列表
    List<OrderInfo> findUserPage(Long userId, Integer orderStatus);

    // 根据订单编号获取订单信息
    OrderInfo getOrderInfoByOrderNo(String orderNo);

    // 根据id更新数据
    void updateById(OrderInfo orderInfo);
}
