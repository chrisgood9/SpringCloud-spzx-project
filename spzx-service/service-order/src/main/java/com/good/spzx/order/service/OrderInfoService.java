package com.good.spzx.order.service;

import com.github.pagehelper.PageInfo;
import com.good.spzx.model.dto.h5.OrderInfoDto;
import com.good.spzx.model.entity.order.OrderInfo;
import com.good.spzx.model.vo.h5.TradeVo;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/28 14:13
 */
public interface OrderInfoService {

    // 下单(结算) - 获取结算总金额和结算的商品列表
    TradeVo getTrade();

    // 提交订单
    Long submitOrder(OrderInfoDto orderInfoDto);

    // 根据订单id获取订单信息
    OrderInfo getOrderInfoById(Long orderId);

    // 立即购买
    TradeVo buy(Long skuId);

    // 获取订单分页列表
    PageInfo<OrderInfo> findUserPage(Integer page, Integer limit, Integer orderStatus);

    // 根据订单编号获取订单信息
    OrderInfo getOrderInfoByOrderNo(String orderNo);

    // 更新订单状态
    void updateOrderStatus(String orderNo);
}
