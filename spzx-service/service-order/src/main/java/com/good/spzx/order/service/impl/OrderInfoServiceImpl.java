package com.good.spzx.order.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.good.spzx.exception.MyException;
import com.good.spzx.feign.cart.CartFeignClient;
import com.good.spzx.feign.product.ProductFeignClient;
import com.good.spzx.feign.user.UserFeignClient;
import com.good.spzx.model.dto.h5.OrderInfoDto;
import com.good.spzx.model.entity.h5.CartInfo;
import com.good.spzx.model.entity.order.OrderInfo;
import com.good.spzx.model.entity.order.OrderItem;
import com.good.spzx.model.entity.order.OrderLog;
import com.good.spzx.model.entity.product.ProductSku;
import com.good.spzx.model.entity.user.UserAddress;
import com.good.spzx.model.entity.user.UserInfo;
import com.good.spzx.model.vo.common.ResultCodeEnum;
import com.good.spzx.model.vo.h5.TradeVo;
import com.good.spzx.order.mapper.OrderInfoMapper;
import com.good.spzx.order.mapper.OrderItemMapper;
import com.good.spzx.order.mapper.OrderLogMapper;
import com.good.spzx.order.service.OrderInfoService;
import com.good.spzx.utils.AuthContextUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/28 14:13
 */
@Service
public class OrderInfoServiceImpl implements OrderInfoService {

    @Autowired
    private OrderInfoMapper orderInfoMapper;

    @Autowired
    private OrderItemMapper orderItemMapper;

    @Autowired
    private OrderLogMapper orderLogMapper;

    @Autowired
    private CartFeignClient cartFeignClient;

    @Autowired
    private ProductFeignClient productFeignClient;

    @Autowired
    private UserFeignClient userFeignClient;

    /**
     * 下单 - 结算
     * 获取购物车所有选中的商品 <br/>
     * 将购物项数据转换成功订单明细数据 <br/>
     * 计算总金额 <br/>
     * 封装成结算对象并返回
     *
     * @return
     */
    @Override
    public TradeVo getTrade() {
        List<CartInfo> cartInfoList = cartFeignClient.getAllChecked().getData();
        // 存放订单明细数据 - 结算商品列表
        List<OrderItem> orderItemList = new ArrayList<>();
        // 结算总金额
        BigDecimal totalAmount = new BigDecimal(0);
        for (CartInfo cartInfo : cartInfoList) {
            // 转换 -> 添加数据
            OrderItem orderItem = new OrderItem();
            orderItem.setSkuId(cartInfo.getSkuId());
            orderItem.setSkuName(cartInfo.getSkuName());
            orderItem.setSkuNum(cartInfo.getSkuNum());
            orderItem.setSkuPrice(cartInfo.getCartPrice());
            orderItem.setThumbImg(cartInfo.getImgUrl());
            orderItemList.add(orderItem);
            // 计算 -> 结算总金额
            totalAmount = totalAmount.add(cartInfo.getCartPrice().multiply(new BigDecimal(cartInfo.getSkuNum())));
        }
        return new TradeVo(totalAmount, orderItemList);
    }

    /**
     * 提交订单 - 返回订单id
     * 这里的库存量是在支付的时候进行减
     * 涉及三张表
     * 1. 从参数中获取所有订单项 (orderItemList)
     * 2. 每个订单项 (orderItem) 校验商品库存
     * 获取商品sku信息(可以获取到库存量)进而判断库存量是否充足
     * 4. 封装数据 -> 添加数据到 order_info 表
     * (获取用户收货地址信息)
     * 5. 添加订单项数据添加到 order_item 表中
     * 6. 添加数据到 order_log表
     * 7. 在购物车中删除已经生成订单的商品
     * 8. 返回订单id
     *
     * @param orderInfoDto
     * @return
     */
    @Override
    public Long submitOrder(OrderInfoDto orderInfoDto) {
        List<OrderItem> orderItemList = orderInfoDto.getOrderItemList();
        if (CollectionUtils.isEmpty(orderItemList) && orderItemList.size() == 0) {
            throw new MyException(ResultCodeEnum.DATA_ERROR);
        }
        // 遍历判断订单项 - 校验库存
        orderItemList.stream().forEach(orderItem -> {
            ProductSku productSku = (ProductSku) productFeignClient.getProductBySkuId(orderItem.getSkuId()).getData();
            if (ObjectUtil.isEmpty(productSku)) {
                throw new MyException(ResultCodeEnum.DATA_ERROR);
            }
            if (orderItem.getSkuNum() > productSku.getStockNum()) {
                throw new MyException(ResultCodeEnum.STOCK_LESS);
            }
        });

        // 添加订单信息
        UserInfo userInfo = AuthContextUtil.getUserInfo();
        OrderInfo orderInfo = new OrderInfo();
        orderInfo.setUserId(userInfo.getId()); // 用户id
        orderInfo.setNickName(userInfo.getNickName()); // 用户昵称
        orderInfo.setOrderNo(String.valueOf(System.currentTimeMillis())); // 订单编号 - 用时间戳来保证唯一
        // 封装收货地址信息
        UserAddress userAddress = userFeignClient.getUserAddress(orderInfoDto.getUserAddressId());
        orderInfo.setReceiverName(userAddress.getName());
        orderInfo.setReceiverPhone(userAddress.getPhone());
        orderInfo.setReceiverTagName(userAddress.getTagName());
        orderInfo.setReceiverProvince(userAddress.getProvinceCode());
        orderInfo.setReceiverCity(userAddress.getCityCode());
        orderInfo.setReceiverDistrict(userAddress.getDistrictCode());
        orderInfo.setReceiverAddress(userAddress.getFullAddress());
        //订单金额
        BigDecimal totalAmount = new BigDecimal(0);
        for (OrderItem orderItem : orderItemList) {
            totalAmount = totalAmount.add(orderItem.getSkuPrice().multiply(new BigDecimal(orderItem.getSkuNum())));
        }
        orderInfo.setTotalAmount(totalAmount);
        orderInfo.setCouponAmount(new BigDecimal(0));
        orderInfo.setOriginalTotalAmount(totalAmount);
        orderInfo.setFeightFee(orderInfoDto.getFeightFee());
        orderInfo.setPayType(2);
        orderInfo.setOrderStatus(0);
        // 最终添加OrderInfo
        orderInfoMapper.save(orderInfo);

        // 添加orderItem
        for (OrderItem orderItem : orderItemList) {
            orderItem.setOrderId(orderInfo.getId());
            orderItemMapper.save(orderItem);
        }

        //记录日志
        OrderLog orderLog = new OrderLog();
        orderLog.setOrderId(orderInfo.getId());
        orderLog.setProcessStatus(0);
        orderLog.setNote("提交订单");
        orderLogMapper.save(orderLog);

        // 清空购物车中已经生成订单的的数据
        cartFeignClient.deleteChecked();
        return orderInfo.getId();
    }

    @Override
    public OrderInfo getOrderInfoById(Long orderId) {
        return orderInfoMapper.getOrderInfoById(orderId);
    }

    @Override
    public TradeVo buy(Long skuId) {
        List<OrderItem> orderItemList = new ArrayList<>();
        // 获取sku信息
        ProductSku productSku = (ProductSku) productFeignClient.getProductBySkuId(skuId).getData();
        // 封装数据
        OrderItem orderItem = new OrderItem();
        orderItem.setSkuId(skuId);
        orderItem.setSkuName(productSku.getSkuName());
        orderItem.setSkuNum(1); // 立即购买 -> 默认是添加一件商品
        orderItem.setSkuPrice(productSku.getSalePrice());
        orderItem.setThumbImg(productSku.getThumbImg());
        orderItemList.add(orderItem);

        return new TradeVo(orderItem.getSkuPrice(), orderItemList);
    }

    @Override
    public PageInfo<OrderInfo> findUserPage(Integer page, Integer limit, Integer orderStatus) {
        PageHelper.startPage(page, limit);
        // 获取当前用户id的订单信息列表
        Long userId = AuthContextUtil.getUserInfo().getId();
        List<OrderInfo> orderInfoList = orderInfoMapper.findUserPage(userId, orderStatus);
        // 设置每个订单中的订单项信息
        orderInfoList.stream().forEach(orderInfo -> {
            // 根据订单id获取订单项信息 -> 添加到当前订单
            List<OrderItem> orderItemList = orderItemMapper.getItemByOrderId(orderInfo.getId());
            orderInfo.setOrderItemList(orderItemList);
        });
        return new PageInfo<>(orderInfoList);
    }

    @Override
    public OrderInfo getOrderInfoByOrderNo(String orderNo) {
        return orderInfoMapper.getOrderInfoByOrderNo(orderNo);
    }

    @Override
    public void updateOrderStatus(String orderNo) {
        // 更细orderInfo数据
        OrderInfo orderInfo = orderInfoMapper.getOrderInfoByOrderNo(orderNo);
        orderInfo.setOrderStatus(1); // 设置成已支付状态 - 代发货状态
        orderInfo.setPaymentTime(new Date());
        orderInfo.setPayType(2); // 支付状态- 这里写死用支付宝支付
        orderInfoMapper.updateById(orderInfo);

        // 记录日志
        OrderLog orderLog = new OrderLog();
        orderLog.setOrderId(orderInfo.getId());
        orderLog.setProcessStatus(1);
        orderLog.setNote("支付宝支付成功");
        orderLogMapper.save(orderLog);
    }

}
