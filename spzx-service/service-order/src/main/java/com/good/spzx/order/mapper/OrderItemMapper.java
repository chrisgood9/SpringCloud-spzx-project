package com.good.spzx.order.mapper;

import com.good.spzx.model.entity.order.OrderItem;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/29 15:25
 */
@Mapper
public interface OrderItemMapper {

    void save(OrderItem orderItem);

    // 根据订单id获取订单项信息
    List<OrderItem> getItemByOrderId(Long orderId);

}
