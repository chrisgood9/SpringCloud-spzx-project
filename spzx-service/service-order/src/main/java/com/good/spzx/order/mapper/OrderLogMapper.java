package com.good.spzx.order.mapper;

import com.good.spzx.model.entity.order.OrderLog;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/29 15:26
 */
@Mapper
public interface OrderLogMapper {

    void save(OrderLog orderLog);

}
