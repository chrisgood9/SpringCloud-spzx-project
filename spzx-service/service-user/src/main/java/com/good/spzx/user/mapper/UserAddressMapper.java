package com.good.spzx.user.mapper;

import com.good.spzx.model.entity.user.UserAddress;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/28 14:25
 */
@Mapper
public interface UserAddressMapper {

    // 根据用户id查询地址列表
    List<UserAddress> findUserAddressListByUserId(Long userId);

    // 根据用户id获取地址信息
    UserAddress getById(Long id);
}