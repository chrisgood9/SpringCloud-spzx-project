package com.good.spzx.user.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSON;
import com.good.spzx.exception.MyException;
import com.good.spzx.model.dto.h5.UserLoginDto;
import com.good.spzx.model.dto.h5.UserRegisterDto;
import com.good.spzx.model.entity.user.UserInfo;
import com.good.spzx.model.vo.common.ResultCodeEnum;
import com.good.spzx.model.vo.h5.UserInfoVo;
import com.good.spzx.user.mapper.UserInfoMapper;
import com.good.spzx.user.service.UserInfoService;
import com.good.spzx.utils.AuthContextUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.DigestUtils;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/27 9:48
 */
@Service
public class UserInfoServiceImpl implements UserInfoService {

    // 用户登录token前缀
    public static final String USER_TOKEN_PREFIX = "userLogin:info";

    @Autowired
    private UserInfoMapper userInfoMapper;

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    /**
     * 这里的注册，仅仅支持手机号注册 o.O
     *
     * @param userRegisterDto
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void register(UserRegisterDto userRegisterDto) {
        // 1. 获取传入的数据
        String code = userRegisterDto.getCode();
        String username = userRegisterDto.getUsername();
        String password = userRegisterDto.getPassword();
        String nickName = userRegisterDto.getNickName();
        //校验参数
        if (StringUtils.isEmpty(username) ||
                StringUtils.isEmpty(password) ||
                StringUtils.isEmpty(nickName) ||
                StringUtils.isEmpty(code)) {
            throw new MyException(ResultCodeEnum.DATA_ERROR);
        }
        // 2. 校验验证码
        String redisCode = redisTemplate.opsForValue().get(username);
        if (!redisCode.equals(code)) {
            throw new MyException(ResultCodeEnum.VALIDATECODE_ERROR);
        }
        // 3. 校验用户名
        UserInfo userInfo = userInfoMapper.selectUserByName(username);
        if (!ObjectUtil.isEmpty(userInfo)) {
            throw new MyException(ResultCodeEnum.USER_NAME_IS_EXISTS);
        }

        // 4. 封装添加数据
        userInfo = new UserInfo();
        userInfo.setUsername(username);
        userInfo.setNickName(nickName);
        userInfo.setPassword(DigestUtils.md5DigestAsHex(password.getBytes()));
        userInfo.setPhone(username);
        userInfo.setStatus(1);
        userInfo.setSex(0);
        userInfo.setAvatar("http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eoj0hHXhgJNOTSOFsS4uZs8x1ConecaVOB8eIl115xmJZcT4oCicvia7wMEufibKtTLqiaJeanU2Lpg3w/132");
        userInfoMapper.save(userInfo);

        // 删除Redis中的数据
        redisTemplate.delete(username);
    }

    /**
     * 登录采用用户名与密码登录方式，
     * 登录成功将 用户信息保存到redis，并生成token返回，
     * 前端H5会把token信息保存到浏览器本地存储，
     * 后续访问接口默认将token带在header头进行访问
     *
     * @param userLoginDto
     */
    @Override
    public String login(UserLoginDto userLoginDto) {
        String username = userLoginDto.getUsername();
        String password = userLoginDto.getPassword();
        // 校验参数
        if (StringUtils.isEmpty(username) || StringUtils.isEmpty(password)) {
            throw new MyException(ResultCodeEnum.DATA_ERROR);
        }

        UserInfo userInfo = userInfoMapper.selectUserByName(username);
        // 用户不存在
        if (ObjectUtil.isEmpty(userInfo)) {
            throw new MyException(ResultCodeEnum.LOGIN_ERROR);
        }
        // 校验密码
        String passwordParse = DigestUtils.md5DigestAsHex(password.getBytes());
        if (!userInfo.getPassword().equals(passwordParse)) {
            throw new MyException(ResultCodeEnum.LOGIN_ERROR);
        }
        // 判断用户是否是禁用状态
        if (userInfo.getStatus() == 0) {
            throw new MyException(ResultCodeEnum.ACCOUNT_STOP);
        }
        // 生成token信息并返回，添加用户信息到缓存
        String token = UUID.randomUUID().toString().replaceAll("-", "");
        redisTemplate.opsForValue().set(USER_TOKEN_PREFIX + token,
                JSON.toJSONString(userInfo),
                5, TimeUnit.DAYS);
        return token;
    }

    /**
     * 登陆后，根据token信息，在redis中获取用户信息
     *
     * @param token
     */
    @Override
    public UserInfoVo getCurrentUserInfo(String token) {
        // 基于redis方法获取：
//        String userJsonInfo = redisTemplate.opsForValue().get(USER_TOKEN_PREFIX + token);
//        if (StringUtils.isEmpty(userJsonInfo)) {
//            throw new MyException(ResultCodeEnum.LOGIN_AUTH);
//        }
//        UserInfo userInfo = JSON.parseObject(userJsonInfo, UserInfo.class);

        // 配置了拦截器 保存了userInfo信息 -> 从ThreadLocal中获取
        UserInfo userInfo = AuthContextUtil.getUserInfo();
        UserInfoVo userInfoVo = new UserInfoVo();
        BeanUtils.copyProperties(userInfo, userInfoVo);
        return userInfoVo;
    }

}
