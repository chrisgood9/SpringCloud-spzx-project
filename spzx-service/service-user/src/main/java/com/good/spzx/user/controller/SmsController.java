package com.good.spzx.user.controller;

import com.good.spzx.model.vo.common.Result;
import com.good.spzx.model.vo.common.ResultCodeEnum;
import com.good.spzx.user.service.SmsService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/26 22:54
 */
@Tag(name = "用户验证码接口")
@RestController
@RequestMapping("/api/user/sms")
public class SmsController {

    @Autowired
    private SmsService smsService;

    /**
     * 给指定手机号发送短信验证码
     *
     * @param phone
     * @return
     */
    @GetMapping(value = "/sendCode/{phone}")
    public Result sendValidateCode(@PathVariable("phone") String phone) {
        smsService.sendValidateCode(phone);
        return Result.build(null, ResultCodeEnum.SUCCESS);
    }

}
