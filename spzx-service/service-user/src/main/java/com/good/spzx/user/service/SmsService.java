package com.good.spzx.user.service;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/26 22:55
 */
public interface SmsService {
    // 给指定手机号发送短信验证码
    void sendValidateCode(String phone);
}
