package com.good.spzx.user.mapper;

import com.good.spzx.model.entity.user.UserInfo;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.data.repository.query.Param;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/27 9:48
 */
@Mapper
public interface UserInfoMapper {

    // 根据用户名查询用户信息
    UserInfo selectUserByName(@Param("username") String username);

    // 添加用户
    void save(UserInfo userInfo);
}
