package com.good.spzx.user.controller;

import com.good.spzx.model.dto.h5.UserLoginDto;
import com.good.spzx.model.dto.h5.UserRegisterDto;
import com.good.spzx.model.vo.common.Result;
import com.good.spzx.model.vo.common.ResultCodeEnum;
import com.good.spzx.model.vo.h5.UserInfoVo;
import com.good.spzx.user.service.UserInfoService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/27 9:46
 */
@Tag(name = "会员用户接口")
@RestController
@RequestMapping("/api/user/userInfo")
public class UserInfoController {

    @Autowired
    private UserInfoService userInfoService;

    /**
     * 会员注册
     *
     * @param userRegisterDto
     * @return
     */
    @Operation(summary = "会员注册")
    @PostMapping(value = "/register")
    public Result register(@RequestBody UserRegisterDto userRegisterDto) {
        userInfoService.register(userRegisterDto);
        return Result.build(null, ResultCodeEnum.SUCCESS);
    }

    /**
     * 会员登录
     *
     * @param userLoginDto
     * @return
     */
    @Operation(summary = "会员登录")
    @PostMapping("/login")
    public Result login(@RequestBody UserLoginDto userLoginDto) {
        return Result.build(userInfoService.login(userLoginDto), ResultCodeEnum.SUCCESS);
    }

    /**
     * 获取当前登录用户信息
     *
     * @param request
     * @return
     */
    @GetMapping("/auth/getCurrentUserInfo")
    public Result getCurrentUserInfo(HttpServletRequest request) {
        String token = request.getHeader("token");
        UserInfoVo userInfoVo = userInfoService.getCurrentUserInfo(token);
        return Result.build(userInfoVo, ResultCodeEnum.SUCCESS);
    }

}
