package com.good.spzx.user.controller;

import com.good.spzx.model.entity.user.UserAddress;
import com.good.spzx.model.vo.common.Result;
import com.good.spzx.model.vo.common.ResultCodeEnum;
import com.good.spzx.user.service.UserAddressService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/28 14:07
 */
@Tag(name = "用户地址接口")
@RestController
@RequestMapping(value = "/api/user/userAddress")
public class UserAddressController {

    @Autowired
    private UserAddressService userAddressService;

    /**
     * 获取当前用户的地址列表
     *
     * @return
     */
    @Operation(summary = "获取用户地址列表")
    @GetMapping("auth/findUserAddressList")
    public Result<List<UserAddress>> findUserAddressList() {
        List<UserAddress> list = userAddressService.findUserAddressList();
        return Result.build(list, ResultCodeEnum.SUCCESS);
    }


    /**
     * 根据用户id获取地址信息
     *
     * @param id
     * @return
     */
    @Operation(summary = "获取地址信息")
    @GetMapping("getUserAddress/{id}")
    public UserAddress getUserAddress(@PathVariable Long id) {
        return userAddressService.getById(id);
    }
}
