package com.good.spzx.user.service;

import com.good.spzx.model.dto.h5.UserLoginDto;
import com.good.spzx.model.dto.h5.UserRegisterDto;
import com.good.spzx.model.entity.user.UserInfo;
import com.good.spzx.model.vo.h5.UserInfoVo;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/27 9:48
 */
public interface UserInfoService {
    // 用户注册
    void register(UserRegisterDto userRegisterDto);

    // 登录
    String login(UserLoginDto userLoginDto);

    // 获取当前登录用户信息
    UserInfoVo getCurrentUserInfo(String token);
}
