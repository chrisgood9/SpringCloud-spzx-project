package com.good.spzx.user;

import com.good.spzx.annotation.EnableUserLoginAuthInterceptor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/26 22:49
 */
@SpringBootApplication
@ComponentScan(basePackages = {"com.good.spzx"})
@EnableDiscoveryClient
@EnableUserLoginAuthInterceptor
public class UserApplication {
    public static void main(String[] args) {
        SpringApplication.run(UserApplication.class, args);
    }
}
