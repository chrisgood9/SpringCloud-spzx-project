package com.good.spzx.user.service;

import com.good.spzx.model.entity.user.UserAddress;

import java.util.List;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/28 14:25
 */
public interface UserAddressService {

    // 获取用户地址列表
    List<UserAddress> findUserAddressList();

    // 根据用户id获取地址信息
    UserAddress getById(Long id);
}
