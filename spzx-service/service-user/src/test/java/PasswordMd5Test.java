import org.springframework.util.DigestUtils;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/27 14:51
 */
public class PasswordMd5Test {

    public static void main(String[] args) {
        testMD5();
    }

    public static void testMD5() {
        System.out.println(DigestUtils.md5DigestAsHex("11111111".getBytes()));
    }

}
