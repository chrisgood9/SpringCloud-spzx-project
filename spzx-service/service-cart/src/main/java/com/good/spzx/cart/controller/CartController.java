package com.good.spzx.cart.controller;

import com.good.spzx.cart.service.CartService;
import com.good.spzx.model.entity.h5.CartInfo;
import com.good.spzx.model.vo.common.Result;
import com.good.spzx.model.vo.common.ResultCodeEnum;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/27 23:01
 */
@RestController
@RequestMapping("/api/order/cart")
public class CartController {

    @Autowired
    private CartService cartService;

    /**
     * 添加购物车
     *
     * @param skuId
     * @param skuNum
     * @return
     */
    @Operation(summary = "添加购物车")
    @GetMapping("auth/addToCart/{skuId}/{skuNum}")
    public Result addToCart(@PathVariable("skuId") Long skuId,
                            @PathVariable("skuNum") Integer skuNum) {
        cartService.addToCart(skuId, skuNum);
        return Result.build(null, ResultCodeEnum.SUCCESS);
    }

    /**
     * 查询购物车列表
     *
     * @return
     */
    @Operation(summary = "查询购物车")
    @GetMapping("auth/cartList")
    public Result getCartList() {
        List<CartInfo> cartList = cartService.getCartList();
        return Result.build(cartList, ResultCodeEnum.SUCCESS);
    }

    /**
     * 删除购物车商品
     *
     * @param skuId
     * @return
     */
    @Operation(summary = "删除购物车商品")
    @DeleteMapping("auth/deleteCart/{skuId}")
    public Result deleteCart(@PathVariable("skuId") Long skuId) {
        cartService.deleteCart(skuId);
        return Result.build(null, ResultCodeEnum.SUCCESS);
    }

    /**
     * 更新购物车商品选中状态
     *
     * @param skuId
     * @param isChecked
     * @return
     */
    @Operation(summary = "更新购物车商品选中状态")
    @GetMapping("/auth/checkCart/{skuId}/{isChecked}")
    public Result checkCart(@PathVariable(value = "skuId") Long skuId,
                            @PathVariable(value = "isChecked") Integer isChecked) {
        cartService.checkCart(skuId, isChecked);
        return Result.build(null, ResultCodeEnum.SUCCESS);
    }

    /**
     * 更新购物车商品全部选中状态
     *
     * @param isChecked
     * @return
     */
    @Operation(summary = "更新购物车商品全部选中状态")
    @GetMapping("/auth/allCheckCart/{isChecked}")
    public Result allCheckCart(@PathVariable(value = "isChecked") Integer isChecked) {
        cartService.allCheckCart(isChecked);
        return Result.build(null, ResultCodeEnum.SUCCESS);
    }

    /**
     * 清空购物车
     *
     * @return
     */
    @Operation(summary = "清空购物车")
    @GetMapping("/auth/clearCart")
    public Result clearCart() {
        cartService.clearCart();
        return Result.build(null, ResultCodeEnum.SUCCESS);
    }

    /**
     * 获取选中的购物车中的商品信息
     *
     * @return
     */
    @Operation(summary = "选中的购物车")
    @GetMapping(value = "/auth/getAllCkecked")
    public Result<List<CartInfo>> getAllChecked() {
        List<CartInfo> cartInfoList = cartService.getAllChecked();
        return Result.build(cartInfoList, ResultCodeEnum.SUCCESS);
    }

    /**
     * 购物车中删除已经生成订单的数据
     *
     * @return
     */
    @GetMapping(value = "/auth/deleteChecked")
    public Result deleteChecked() {
        cartService.deleteChecked();
        return Result.build(null, ResultCodeEnum.SUCCESS);
    }
}
