package com.good.spzx.cart.service;

import com.good.spzx.model.entity.h5.CartInfo;

import java.util.List;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/27 23:02
 */
public interface CartService {
    // 添加购物车
    void addToCart(Long skuId, Integer skuNum);

    // 获取购物车列表信息
    List<CartInfo> getCartList();

    // 删除购物车商品
    void deleteCart(Long skuId);

    // 更新购物车商品选中状态
    void checkCart(Long skuId, Integer isChecked);

    // 更新购物车商品全部选中状态
    void allCheckCart(Integer isChecked);

    // 清空购物车
    void clearCart();

    // 获取选中的购物车
    List<CartInfo> getAllChecked();

    // 购物车中删除已经生成订单的数据
    void deleteChecked();

}
