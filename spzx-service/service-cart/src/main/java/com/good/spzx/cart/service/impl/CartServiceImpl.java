package com.good.spzx.cart.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson2.JSON;
import com.good.spzx.cart.service.CartService;
import com.good.spzx.feign.product.ProductFeignClient;
import com.good.spzx.model.entity.h5.CartInfo;
import com.good.spzx.model.entity.product.ProductSku;
import com.good.spzx.utils.AuthContextUtil;
import jakarta.annotation.Resource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/27 23:03
 */
@Service
public class CartServiceImpl implements CartService {

    // key = user:cart:userId
    public static final String CART_KEY = "user:cart:";

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @Autowired
    private ProductFeignClient productFeignClient;

    @Override
    public void addToCart(Long skuId, Integer skuNum) {
        // 获取当前用户id信息
        Long userId = AuthContextUtil.getUserInfo().getId();
        String cartKey = this.getCartKey(userId);

        // (使用Hash来存储购物车数据 key field value)
        // 判断是否已经添加过该商品: field是skuId，value是商品详情信息
        // -> 已存在：数量+skuNum，不存在：从商品详情表中获取数据添加信息到redis
        Object cartInfoObj = redisTemplate.opsForHash().get(cartKey, String.valueOf(skuId));
        CartInfo cartInfo = null;
        if (!ObjectUtil.isEmpty(cartInfoObj)) {
            cartInfo = JSON.parseObject(cartInfoObj.toString(), CartInfo.class);
            cartInfo.setSkuNum(cartInfo.getSkuNum() + skuNum);
            cartInfo.setIsChecked(1);
            cartInfo.setUpdateTime(new Date());
        } else {
            cartInfo = new CartInfo();
            // 购物车数据是从商品详情得到 {skuInfo}
            ProductSku productSkuInfo = (ProductSku) productFeignClient.getProductBySkuId(skuId).getData();
            // 封装数据
            cartInfo.setUserId(userId);
            cartInfo.setSkuId(skuId);
            cartInfo.setSkuNum(skuNum);
            cartInfo.setSkuName(productSkuInfo.getSkuName());
            cartInfo.setCartPrice(productSkuInfo.getSalePrice());
            cartInfo.setImgUrl(productSkuInfo.getThumbImg());
            cartInfo.setIsChecked(1);
            cartInfo.setCreateTime(new Date());
            cartInfo.setUpdateTime(new Date());
        }

        // 存入redis
        redisTemplate.opsForHash().put(cartKey, String.valueOf(skuId), JSON.toJSONString(cartInfo));
    }

    @Override
    public List<CartInfo> getCartList() {
        // 从redis中获取购物车信息
        Long userId = AuthContextUtil.getUserInfo().getId();
        String cartKey = this.getCartKey(userId);
        // 获取所有value值
        List<Object> cartValues = redisTemplate.opsForHash().values(cartKey);
        if (!CollectionUtil.isEmpty(cartValues)) {
            // 用Stream流的方式进行转换
            List<CartInfo> cartList = cartValues.stream().map(cartInfoObj
                    -> JSON.parseObject(cartInfoObj.toString(), CartInfo.class))
                    // 用比较器来对创建时间进行排列
                    .sorted(((o1, o2) -> o2.getCreateTime().compareTo(o1.getCreateTime())))
                    .collect(Collectors.toList());
            return cartList;
        }
        return new ArrayList<>();
    }

    @Override
    public void deleteCart(Long skuId) {
        String cartKey = this.getCartKey(AuthContextUtil.getUserInfo().getId());
        redisTemplate.opsForHash().delete(cartKey, String.valueOf(skuId));
    }

    @Override
    public void checkCart(Long skuId, Integer isChecked) {
        String cartKey = this.getCartKey(AuthContextUtil.getUserInfo().getId());
        // 判断field中是否存在该skuId -> 存在：更新选中状态
        if (redisTemplate.opsForHash().hasKey(cartKey, String.valueOf(skuId))) {
            Object cartInfoObj = redisTemplate.opsForHash().get(cartKey, skuId);
            CartInfo cartInfo = JSON.parseObject(cartInfoObj.toString(), CartInfo.class);
            cartInfo.setIsChecked(isChecked);
            // 更新redis数据
            redisTemplate.opsForHash().put(cartKey, String.valueOf(skuId), JSON.toJSONString(cartInfo));
        }
    }

    @Override
    public void allCheckCart(Integer isChecked) {
        String cartKey = this.getCartKey(AuthContextUtil.getUserInfo().getId());
        // 获取所有的购物项数据
        List<Object> values = redisTemplate.opsForHash().values(cartKey);
        if (!CollectionUtil.isEmpty(values)) {
            // 用steam流来进行更新状态 - 全选
            values.stream().map(cartInfoObj -> {
                CartInfo cartInfo = JSON.parseObject(cartInfoObj.toString(), CartInfo.class);
                cartInfo.setIsChecked(isChecked);
                return cartInfo;
            }).forEach(cartInfo ->
                    redisTemplate.opsForHash()
                            .put(cartKey, cartInfo.getSkuId(), JSON.toJSONString(cartInfo)));
        }
    }
// 这是另外一种写法，上面是直接操作流而不必再额外创建集合，节省了内存空间和对对象的额外处理
//    List<CartInfo> collect = values.stream().map(cartInfoObj -> {
//        CartInfo cartInfo = JSON.parseObject(cartInfoObj.toString(), CartInfo.class);
//        cartInfo.setIsChecked(isChecked);
//        return cartInfo;
//    }).collect(Collectors.toList());
//            collect.forEach(cartInfo ->
//            redisTemplate.opsForHash()
//            .put(cartKey, cartInfo.getSkuId(), JSON.toJSONString(cartInfo)));

    @Override
    public void clearCart() {
        String cartKey = this.getCartKey(AuthContextUtil.getUserInfo().getId());
        redisTemplate.opsForHash().delete(cartKey);
    }

    @Override
    public List<CartInfo> getAllChecked() {
        String cartKey = this.getCartKey(AuthContextUtil.getUserInfo().getId());
        List<Object> values = redisTemplate.opsForHash().values(cartKey);
        if (!CollectionUtil.isEmpty(values)) {
            List<CartInfo> cartInfoList = values.stream().map(cartInfoObj -> {
                CartInfo cartInfo = JSON.parseObject(cartInfoObj.toString(), CartInfo.class);
                return cartInfo;
            }).filter(cartInfo -> cartInfo.getIsChecked() == 1) // 过滤选中状态 = 1 的商品
                    .collect(Collectors.toList());
            return cartInfoList;
        }
        return new ArrayList<>();
    }

    @Override
    public void deleteChecked() {
        String cartKey = this.getCartKey(AuthContextUtil.getUserInfo().getId());
        List<Object> values = redisTemplate.opsForHash().values(cartKey);
        if (!CollectionUtil.isEmpty(values)) {
            values.stream()
                    .map(cartInfo ->
                            JSON.parseObject(cartInfo.toString(), CartInfo.class))
                    .filter(cartInfo ->
                            cartInfo.getIsChecked() == 1)
                    .forEach(cartInfo ->
                            redisTemplate.opsForHash()
                                    .delete(cartKey, String.valueOf(cartInfo.getSkuId())));
        }
    }

    /**
     * 构建redis - cartKey
     * <br/> cartKey = CART_KEY + userId
     * <br/> CART_KEY(固定前缀) = user:cart:
     * <br/> cartKey = user:cart:userId
     *
     * @param userId
     * @return
     */
    private String getCartKey(Long userId) {
        return CART_KEY + userId;
    }
}
