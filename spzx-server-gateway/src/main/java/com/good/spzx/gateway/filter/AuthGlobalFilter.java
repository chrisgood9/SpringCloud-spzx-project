package com.good.spzx.gateway.filter;

import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.good.spzx.model.entity.user.UserInfo;
import com.good.spzx.model.vo.common.Result;
import com.good.spzx.model.vo.common.ResultCodeEnum;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.StringUtils;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.nio.charset.StandardCharsets;
import java.util.List;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/27 19:04
 */

/**
 * 全局过滤器
 */
@Component
public class AuthGlobalFilter implements GlobalFilter, Ordered {

    // 用户登录token前缀
    public static final String USER_TOKEN_PREFIX = "userLogin:info";

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    // AntPathMatcher 是 Spring 框架中的一个类，用于进行路径匹配的工具类之一。
    // 它通常用于处理 URL 或者文件路径的模式匹配。
    private AntPathMatcher antPathMatcher = new AntPathMatcher();

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        // 获取请求路径
        ServerHttpRequest request = exchange.getRequest();
        String path = request.getURI().getPath();

        // 判断路径 —— 登录校验(满足规则：/api/**/auth/**)
        if (antPathMatcher.match("/api/**/auth/**", path)) {
            // 路径匹配后 -> 进行登录校验
            UserInfo userInfo = this.getUserInfo(request);
            //  redis中数据不存在 -> 未登录 -> 返回错误信息
            if (ObjectUtil.isEmpty(userInfo)) {
                ServerHttpResponse response = exchange.getResponse();
                return out(response, ResultCodeEnum.LOGIN_AUTH);
            }
        }

        // 放行
        return chain.filter(exchange);
    }

    @Override
    public int getOrder() {
        return 0;
    }

    /**
     * 构建响应的方法。
     * 它的作用是创建一个基于 Reactive 编程模型的方法，
     * 用于向客户端返回一个 JSON 格式的响应。
     *
     * @param response
     * @param resultCodeEnum
     * @return
     */
    private Mono<Void> out(ServerHttpResponse response, ResultCodeEnum resultCodeEnum) {
        Result result = Result.build(null, resultCodeEnum);
        byte[] bits = JSONObject.toJSONString(result).getBytes(StandardCharsets.UTF_8);
        DataBuffer buffer = response.bufferFactory().wrap(bits);
        //指定编码，否则在浏览器中会中文乱码
        response.getHeaders().add("Content-Type", "application/json;charset=UTF-8");
        return response.writeWith(Mono.just(buffer));
    }

    /**
     * 根据请求头的token信息，在redis中获取用户数据
     *
     * @param request
     * @return
     */
    private UserInfo getUserInfo(ServerHttpRequest request) {
        String token = "";
        // ServerHttpRequest 不能只能在请求头中获取，只能如下：
        List<String> tokenList = request.getHeaders().get("token");
        if (!tokenList.isEmpty()) {
            token = tokenList.get(0);
        }
        // 请求头中存在token -> 查询redis - 检验是否存在
        if (StringUtils.hasText(token)) {
            String jsonToken = redisTemplate.opsForValue().get(USER_TOKEN_PREFIX + token);
            if (StringUtils.hasText(jsonToken)) {
                UserInfo userInfo = JSON.parseObject(jsonToken, UserInfo.class);
                return userInfo;
            }
        }
        return null;
    }

}
