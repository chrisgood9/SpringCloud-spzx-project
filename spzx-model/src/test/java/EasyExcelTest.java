import com.alibaba.excel.EasyExcel;
import com.good.spzx.listener.ExcelListener;
import com.good.spzx.model.vo.product.CategoryExcelVo;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/20 13:34
 */
public class EasyExcelTest {
    public static void main(String[] args) {
        testRead();
        write();
    }

    // 读取操作
    public static void testRead() {
        // 文件路径
        String fileName = "D://testRead.xlsx";

        // 创建监听器对象
        ExcelListener<CategoryExcelVo> excelListener = new ExcelListener<>();

        //  - 开始读取 -
        // 解析excel表格 \  sheet() 默认是第一张sheet, 可以指定sheet
        EasyExcel.read(fileName, CategoryExcelVo.class, excelListener).sheet().doRead();
        List<CategoryExcelVo> excelVoList = excelListener.getDatas();
        excelVoList.forEach(categoryExcelVo -> System.out.println(categoryExcelVo));
    }

    // 写入操作
    public static void write() {
        List<CategoryExcelVo> list = new ArrayList<>();
        list.add(new CategoryExcelVo(1L, "数码办公", "", 0L, 1, 1));
        list.add(new CategoryExcelVo(11L, "华为手机", "", 1L, 1, 2));

        String fileName = "D://testWrite.xlsx";
        EasyExcel.write(fileName, CategoryExcelVo.class).sheet("分类数据1").doWrite(list);
        System.out.println("写入完毕");
    }
}
