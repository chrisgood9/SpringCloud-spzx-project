package com.good.spzx.feign;

import feign.RequestInterceptor;
import feign.RequestTemplate;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/28 15:44
 */

/**
 * feign的拦截器， <br/>
 * 因为远程调用不是发送Ajax请求，所以token信息不会传递过去 -> 需要每次远程调用都会携带token信息 <br/>
 * 作用： <br/>
 * 每次使用 Feign 进行远程服务调用时，通过拦截器从当前请求中获取 token，
 * 并将其放入 Feign 请求模板的 Header 中，
 * 以便在远程调用中传递身份验证所需的 token 信息
 */
public class UserTokenOpenFeignInterceptor implements RequestInterceptor {

    @Override
    public void apply(RequestTemplate requestTemplate) {

        // RequestContextHolder.getRequestAttributes() 获取到当前请求的属性。
        // 这里使用了 ServletRequestAttributes 和 HttpServletRequest 对象，用于获取请求的信息。
        ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = requestAttributes.getRequest();
        String token = request.getHeader("token");
        //  将 token 放入 Feign 请求的 Header
        requestTemplate.header("token", token);
    }
}
