package com.good.spzx.exception;

import com.good.spzx.model.vo.common.Result;
import com.good.spzx.model.vo.common.ResultCodeEnum;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/1 15:04
 */

/**
 * 全局异常处理
 *
 * @ControllerAdvice ： Controller增强器，表示对Controller增加统一的操作和处理
 */
@ControllerAdvice
public class GlobalExceptionHandler {

    /**
     * 自定义异常处理
     */
    @ResponseBody
    @ExceptionHandler(value = MyException.class)
    public Result error(MyException myException) {
        return Result.build(null, myException.getResultCodeEnum());
    }

    /**
     * 其他异常处理
     */
    @ResponseBody
    @ExceptionHandler(value = Exception.class)
    public Result error() {
        return Result.build(null, ResultCodeEnum.SYSTEM_ERROR);
    }

}
