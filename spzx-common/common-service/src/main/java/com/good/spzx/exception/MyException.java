package com.good.spzx.exception;

import com.good.spzx.model.vo.common.ResultCodeEnum;
import lombok.Data;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/1 15:21
 */

/**
 * 自定义异常处理类
 * 需要手动抛出该异常
 */
@Data
public class MyException extends RuntimeException {

    private Integer code;

    private String message;

    private ResultCodeEnum resultCodeEnum;

    public MyException(ResultCodeEnum resultCodeEnum) {
        this.resultCodeEnum = resultCodeEnum;
        this.code = resultCodeEnum.getCode();
        this.message = resultCodeEnum.getMessage();
    }

}
