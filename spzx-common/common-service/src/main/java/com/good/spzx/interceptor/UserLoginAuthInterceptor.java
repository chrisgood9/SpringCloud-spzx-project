package com.good.spzx.interceptor;

import com.alibaba.fastjson.JSON;
import com.good.spzx.model.entity.user.UserInfo;
import com.good.spzx.utils.AuthContextUtil;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.servlet.HandlerInterceptor;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/27 20:38
 */

/**
 * 用户登录拦截器，拦截后只是添加数据到 ThreadLocal
 */
public class UserLoginAuthInterceptor implements HandlerInterceptor {

    public static final String USER_TOKEN_PREFIX = "userLogin:info";

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    /**
     * 拦截 api开头的接口，添加用户信息到 ThreadLocal
     *
     * @param request
     * @param response
     * @param handler
     * @return
     * @throws Exception
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {// 拦
        String token = request.getHeader("token");
        String tokenJson = redisTemplate.opsForValue().get(USER_TOKEN_PREFIX + token);
        AuthContextUtil.setUserInfoThreadLocal(JSON.parseObject(tokenJson, UserInfo.class));
        return true;
    }
}
