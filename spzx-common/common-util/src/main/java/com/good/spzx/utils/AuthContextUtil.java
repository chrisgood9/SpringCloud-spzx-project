package com.good.spzx.utils;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/2 14:17
 */

import com.good.spzx.model.entity.system.SysUser;
import com.good.spzx.model.entity.user.UserInfo;

/**
 * ThreadLocal是jdk所提供的一个线程工具类，叫做线程变量，
 * 意思是ThreadLocal中填充的变量属于当前线程，该变量对其他线程而言是隔离的，
 * 也就是说该变量是当前线程独有的变量，使用该工具类可以实现在同一个线程进行数据的共享。
 */
public class AuthContextUtil {
    // 创建一个ThreadLocal对象 - 存放 SysUser
    public static final ThreadLocal<SysUser> threadLocal = new ThreadLocal<>();

    public static final ThreadLocal<UserInfo> userInfoThreadLocal = new ThreadLocal<>();

    // 定义存储数据的静态方法
    public static void set(SysUser sysUser) {
        threadLocal.set(sysUser);
    }

    // 定义获取数据的方法
    public static SysUser get() {
        SysUser sysUser = threadLocal.get();
        return sysUser;
    }

    // 删除数据的方法
    public static void remove() {
        threadLocal.remove();
    }

    public static UserInfo getUserInfo() {
        UserInfo userInfo = userInfoThreadLocal.get();
        return userInfo;
    }

    public static void setUserInfoThreadLocal(UserInfo userInfo) {
        userInfoThreadLocal.set(userInfo);
    }

    // 删除数据的方法
    public static void removeUserInfo() {
        userInfoThreadLocal.remove();
    }
}
