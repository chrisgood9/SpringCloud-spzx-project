package com.good.spzx.common.log.service;

import com.good.spzx.model.entity.system.SysOperLog;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/24 21:39
 */
public interface AsyncOperLogService {

    void saveSysOperLog(SysOperLog sysOperLog);

}
