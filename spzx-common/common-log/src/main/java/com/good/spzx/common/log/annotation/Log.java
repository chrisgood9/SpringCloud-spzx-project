package com.good.spzx.common.log.annotation;

import com.good.spzx.common.log.enums.OperatorType;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/24 20:56
 */

/**
 * 自定义操作日志记录注解
 */
@Target({ElementType.METHOD}) // 作用于method
@Retention(RetentionPolicy.RUNTIME) // 运行时起作用
public @interface Log {

    public String title(); // 模块名称

    public OperatorType operatorType() default OperatorType.MANAGE; // 操作人-类别

    public int businessType(); // 业务类型（0其它 1新增 2修改 3删除）

    public boolean isSaveRequestData() default true; // 是否保存请求的参数

    public boolean isSaveResponseData() default true; // 是否保存响应的参数

}
