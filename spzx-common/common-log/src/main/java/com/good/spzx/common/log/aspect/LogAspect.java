package com.good.spzx.common.log.aspect;

import com.good.spzx.common.log.annotation.Log;
import com.good.spzx.common.log.service.AsyncOperLogService;
import com.good.spzx.common.log.utils.LogUtil;
import com.good.spzx.model.entity.system.SysOperLog;
import lombok.extern.slf4j.Slf4j;
import lombok.extern.slf4j.XSlf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/24 21:05
 */

/**
 * 环绕通知切面类定义
 */
@Aspect
@Slf4j
@Component
public class LogAspect {

    @Autowired
    private AsyncOperLogService asyncOperLogService;

    /**
     * 当使用注解 Log 时，添加环绕通知
     *
     * @param joinPoint
     * @param sysLog
     * @return
     */
    @Around(value = "@annotation(sysLog)")
    public Object doAroundAdvice(ProceedingJoinPoint joinPoint, Log sysLog) {
        // 业务方法之前 - 构建前置参数
        SysOperLog sysOperLog = new SysOperLog();
        LogUtil.beforeHandleLog(sysLog, joinPoint, sysOperLog); //工具类，把信息存入sysOperLog

        // 业务方法
        Object proceed = null;
        try {
            // 执行业务方法
            proceed = joinPoint.proceed();
            // 业务方法之后
            LogUtil.afterHandleLog(sysLog, proceed, sysOperLog, 0, null);
        } catch (Throwable throwable) {
            // 出现异常后
            LogUtil.afterHandleLog(sysLog, proceed, sysOperLog, 1, throwable.getMessage());
            throw new RuntimeException(throwable);
        } finally {
            // 保存日志信息
            asyncOperLogService.saveSysOperLog(sysOperLog);
        }
        // 返回执行结果
        return proceed;
    }


}
