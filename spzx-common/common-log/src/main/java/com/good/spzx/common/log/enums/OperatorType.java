package com.good.spzx.common.log.enums;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/24 21:01
 */

/**
 * 操作人-类别
 */
public enum OperatorType {

    OTHER, // 其他

    MANAGE, // 后台用户

    MOBILE // 手机端用户

}
