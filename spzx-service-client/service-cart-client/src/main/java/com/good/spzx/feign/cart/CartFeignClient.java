package com.good.spzx.feign.cart;

import com.good.spzx.model.entity.h5.CartInfo;
import com.good.spzx.model.vo.common.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/28 15:04
 */
@Component
@FeignClient(value = "service-cart")
public interface CartFeignClient {
    /**
     * 获取选中的购物车中的商品信息
     *
     * @return
     */
    @GetMapping(value = "/api/order/cart/auth/getAllCkecked")
    public Result<List<CartInfo>> getAllChecked();

    /**
     * 在购物车中删除已经生成订单的商品信息
     *
     * @return
     */
    @GetMapping(value = "/api/order/cart/auth/deleteChecked")
    public Result deleteChecked();
}
