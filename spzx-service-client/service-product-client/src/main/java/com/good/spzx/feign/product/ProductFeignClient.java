package com.good.spzx.feign.product;

import com.good.spzx.model.dto.product.SkuSaleDto;
import com.good.spzx.model.vo.common.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/28 12:36
 */
@Component
@FeignClient(value = "service-product")
public interface ProductFeignClient {

    /**
     * 获取商品sku信息
     *
     * @param skuId
     * @return
     */
    @GetMapping("/api/product/getProductBySkuId/{skuId}")
    public Result getProductBySkuId(@PathVariable("skuId") Long skuId);

    /**
     * 更新销量
     *
     * @param skuSaleDtoList
     * @return
     */
    @PostMapping("/api/product/updateSkuSaleNum")
    public Boolean updateSkuSaleNum(@RequestBody List<SkuSaleDto> skuSaleDtoList);
}

