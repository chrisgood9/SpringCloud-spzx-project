package com.good.spzx.feign.user;

import com.good.spzx.model.entity.user.UserAddress;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/29 15:31
 */
@Component
@FeignClient("service-user")
public interface UserFeignClient {
    /**
     * 根据用户id获取用户地址信息
     *
     * @param id
     * @return
     */
    @GetMapping("/api/user/userAddress/getUserAddress/{id}")
    public UserAddress getUserAddress(@PathVariable Long id);

}