package com.good.spzx.feign.order;

import com.good.spzx.model.entity.order.OrderInfo;
import com.good.spzx.model.vo.common.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/29 17:30
 */
@Component
@FeignClient(value = "service-order")
public interface OrderFeignClient {

    /**
     * 根据订单id获取订单信息
     *
     * @param orderNo
     * @return
     */
    @GetMapping("/api/order/orderInfo/auth/getOrderInfoByOrderNo/{orderNo}")
    public Result<OrderInfo> getOrderInfoByOrderNo(@PathVariable("orderNo") String orderNo);

    /**
     * 更新订单状态
     *
     * @param orderNo
     * @return
     */
    @GetMapping("/api/order/orderInfo/auth/updateOrderStatusPayed/{orderNo}")
    public Result updateOrderStatus(@PathVariable("orderNo") String orderNo);
}
