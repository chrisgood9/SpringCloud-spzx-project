package com.good.spzx.manager.controller;

import com.good.spzx.manager.service.SysMenuService;
import com.good.spzx.manager.service.SysUserService;
import com.good.spzx.manager.service.ValidateCodeVoService;
import com.good.spzx.model.dto.system.LoginDto;
import com.good.spzx.model.entity.system.SysUser;
import com.good.spzx.model.vo.common.Result;
import com.good.spzx.model.vo.common.ResultCodeEnum;
import com.good.spzx.model.vo.system.LoginVo;
import com.good.spzx.model.vo.system.SysMenuVo;
import com.good.spzx.model.vo.system.ValidateCodeVo;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @author Chris
 * @version 1.0
 * @date 2023/10/31 16:11
 */
@Tag(name = "用户接口")
@RestController
@RequestMapping(value = "/admin/system/index")
public class IndexController {
    @Autowired
    private SysUserService sysUserService;

    @Autowired
    private ValidateCodeVoService validateCodeVoService;

    @Autowired
    private SysMenuService sysMenuService;

    //用户登录
    @Operation(summary = "登录")
    @PostMapping("login")
    public Result<LoginVo> login(@RequestBody LoginDto LoginDto) {
        LoginVo loginVo = sysUserService.login(LoginDto);
        return Result.build(loginVo, ResultCodeEnum.SUCCESS);
    }

    //用户退出
    @Operation(summary = "用户退出")
    @GetMapping("logout")
    public Result logout(@RequestHeader("token") String token) {
        sysUserService.logout(token);
        return Result.build(null, ResultCodeEnum.SUCCESS);
    }

    //生成验证码图片
    @Operation(summary = "传输验证码")
    @GetMapping("generateValidateCode")
    public Result<ValidateCodeVo> generateValidateCode() {
        ValidateCodeVo validateCodeVo = validateCodeVoService.generateValidateCode();
        return Result.build(validateCodeVo, ResultCodeEnum.SUCCESS);
    }

    //获取用户登录信息
    @GetMapping("getUserInfo")
    public Result getUserInfo(@RequestHeader("token") String token) {
        //方法一：可以传入HttpServletRequest 用getHeader()方法获取请求头中的token信息
        //方法二：使用注解获取请求头中token的信息 @RequestHeader()

        //2. 用token查询redis获取用户信息
        SysUser sysUser = sysUserService.getUserInfo(token);
        //3. 用户信息返回
        return Result.build(sysUser, ResultCodeEnum.SUCCESS);
    }

    /**
     * 查询当前用户可以操作的菜单 —— 实现动态菜单功能
     *
     * @return
     */
    @GetMapping(value = "/menus")
    public Result menus() {
        List<SysMenuVo> menuList = sysMenuService.findUserMenuList();
        return Result.build(menuList, ResultCodeEnum.SUCCESS);
    }

}
