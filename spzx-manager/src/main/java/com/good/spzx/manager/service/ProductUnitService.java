package com.good.spzx.manager.service;

import com.good.spzx.model.entity.base.ProductUnit;

import java.util.List;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/23 14:32
 */
public interface ProductUnitService {

    List<ProductUnit> findAll();

}
