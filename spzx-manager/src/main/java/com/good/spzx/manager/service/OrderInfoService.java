package com.good.spzx.manager.service;

import com.good.spzx.model.dto.order.OrderStatisticsDto;
import com.good.spzx.model.vo.order.OrderStatisticsVo;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/24 17:20
 */
public interface OrderInfoService {
    // 统计查询，根据日期范围 - 获取金额统计数据
    OrderStatisticsVo getOrderStaticsData(OrderStatisticsDto orderStatisticsDto);
}
