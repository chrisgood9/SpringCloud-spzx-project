package com.good.spzx.manager.service;

import com.good.spzx.model.entity.system.SysMenu;
import com.good.spzx.model.vo.system.SysMenuVo;

import java.util.List;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/18 20:16
 */
public interface SysMenuService {

    // 查找所有菜单节点
    List<SysMenu> findNodes();

    // 添加
    void saveMenu(SysMenu sysMenu);

    // 修改
    void updateMenu(SysMenu sysMenu);

    // 删除
    void deletedById(Long menuId);

    // 获取当前用户的菜单
    List<SysMenuVo> findUserMenuList();

}
