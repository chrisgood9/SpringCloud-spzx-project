package com.good.spzx.manager.mapper;

import com.good.spzx.model.dto.product.ProductDto;
import com.good.spzx.model.entity.product.Product;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/23 14:53
 */
@Mapper
public interface ProductMapper {

    // 条件分页查询 - 所有产品信息
    List<Product> findByPage(ProductDto productDto);

    // 保存商品基本信息
    void saveProduct(Product product);

    // 根据id获取商品数据
    Product getProductById(Long id);

    // 修改商品基本信息
    void updateProductById(Product product);

    // 根据id删除商品数据
    void deleteById(Long productId);
}
