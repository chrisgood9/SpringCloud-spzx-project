package com.good.spzx.manager.service;

import com.good.spzx.model.vo.system.ValidateCodeVo;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/1 16:14
 */
public interface ValidateCodeVoService {

    //获取验证码图片,值
    ValidateCodeVo generateValidateCode();

}
