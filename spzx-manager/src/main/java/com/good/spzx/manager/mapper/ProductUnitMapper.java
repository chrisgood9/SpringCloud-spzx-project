package com.good.spzx.manager.mapper;

import com.good.spzx.model.entity.base.ProductUnit;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/23 14:33
 */
@Mapper
public interface ProductUnitMapper {

    List<ProductUnit> findAll();

}
