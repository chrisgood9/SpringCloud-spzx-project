package com.good.spzx.manager.service;

import com.good.spzx.model.entity.product.Category;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/19 20:30
 */
public interface CategoryService {

    // 根据 parentId 获取下级节点
    List<Category> findByParentId(Long parentId);

    // 导出数据到excel表格
    void exportDataToExcel(HttpServletResponse response);

    // 导入数据
    void importData(MultipartFile file);
}
