package com.good.spzx.manager.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.good.spzx.manager.mapper.ProductSpecMapper;
import com.good.spzx.manager.service.ProductSpecService;
import com.good.spzx.model.entity.product.ProductSpec;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/22 14:22
 */
@Service
public class ProductSpecServiceImpl implements ProductSpecService {

    @Autowired
    private ProductSpecMapper productSpecMapper;

    @Override
    public PageInfo<ProductSpec> findByPage(Integer current, Integer limit, ProductSpec productSpec) {
        PageHelper.startPage(current, limit);
        List<ProductSpec> productSpecList = productSpecMapper.findAll();
        PageInfo<ProductSpec> pageInfo = new PageInfo<>(productSpecList);
        return pageInfo;
    }

    @Override
    public void saveSpec(ProductSpec productSpec) {
        productSpecMapper.saveSpec(productSpec);
    }

    @Override
    public void updateSpec(ProductSpec productSpec) {
        productSpecMapper.updateSpec(productSpec);
    }

    @Override
    public void deleteSpec(Long id) {
        productSpecMapper.deleteSpec(id);
    }

    @Override
    public List<ProductSpec> findAll() {
        return productSpecMapper.findAll();
    }
}
