package com.good.spzx.manager.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.good.spzx.manager.mapper.BrandMapper;
import com.good.spzx.manager.service.BrandService;
import com.good.spzx.model.entity.product.Brand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/21 15:13
 */
@Service
public class BrandServiceImpl implements BrandService {

    @Autowired
    private BrandMapper brandMapper;

    @Override
    public PageInfo<Brand> getBrandListByPage(Integer current, Integer limit) {
        PageHelper.startPage(current, limit); // 当前页，每页数量
        List<Brand> brandList = brandMapper.getBrandPage();
        PageInfo<Brand> pageInfo = new PageInfo<>(brandList);
        return pageInfo;
    }

    @Override
    public void addBrand(Brand brand) {
        brandMapper.saveBrand(brand);
    }

    @Override
    public void updateBrand(Brand brand) {
        brandMapper.updateBrand(brand);
    }

    @Override
    public void deleteById(Integer id) {
        brandMapper.deleteById(id);
    }

    @Override
    public List<Brand> findAll() {
        List<Brand> brandPage = brandMapper.getBrandPage();
        return brandPage;
    }
}
