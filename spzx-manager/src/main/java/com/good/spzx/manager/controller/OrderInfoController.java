package com.good.spzx.manager.controller;

import com.good.spzx.manager.service.OrderInfoService;
import com.good.spzx.model.dto.order.OrderStatisticsDto;
import com.good.spzx.model.vo.common.Result;
import com.good.spzx.model.vo.common.ResultCodeEnum;
import com.good.spzx.model.vo.order.OrderStatisticsVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/24 17:19
 */
@RestController
@RequestMapping("/admin/order/orderInfo")
public class OrderInfoController {

    @Autowired
    private OrderInfoService orderInfoService;

    /**
     * 统计查询，根据日期范围 - 获取金额统计数据
     *
     * @param orderStatisticsDto 搜索条件（开始时间 - 结束时间）
     * @return
     */
    @GetMapping(value = "/getOrderStatisticsData")
    public Result getOrderStatisticsData(OrderStatisticsDto orderStatisticsDto) {
        OrderStatisticsVo orderStatisticsVo = orderInfoService.getOrderStaticsData(orderStatisticsDto);
        return Result.build(orderStatisticsVo, ResultCodeEnum.SUCCESS);
    }

}

