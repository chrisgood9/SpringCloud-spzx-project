package com.good.spzx.manager.mapper;

import com.good.spzx.model.dto.system.AssginMenuDto;
import com.good.spzx.model.entity.system.SysMenu;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/19 9:52
 */
@Mapper
public interface SysRoleMenuMapper {

    List<SysMenu> findRoleMenuIds(Long roleId);

    void deletedById(Long roleId);

    void doAssign(AssginMenuDto assginMenuDto);

    // 改成半开状态 => isHalf = 1
    void updateSysRoleMenuIsHalf(Long menuId);
}
