package com.good.spzx.manager.task;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/24 16:35
 */

import cn.hutool.core.date.DateUtil;
import com.good.spzx.manager.mapper.OrderInfoMapper;
import com.good.spzx.manager.mapper.OrderStatisticsMapper;
import com.good.spzx.model.entity.order.OrderStatistics;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 订单统计
 * 定时任务
 */
@Component
public class OrderStatisticsTask {

    @Autowired
    private OrderInfoMapper orderInfoMapper;

    @Autowired
    private OrderStatisticsMapper orderStatisticsMapper;

    /**
     * 每天的凌晨2点 统计前一天的订单金额
     * - 统计后的数据添加到统计结果表
     */
    @Scheduled(cron = "0 0 2 * * ? ")
    public void orderTotalAmountStatistics() {
        // 1. 获取前一天的日期
        String createDate = DateUtil.offsetDay(new Date(), -1).toString("yyyy-MM-dd");
        // 2. 根据前一天的日期进行统计功能（分组操作）
        // 统计前一天交易金额
        OrderStatistics orderStatistics = orderInfoMapper.selectOrderStatisticsByDate(createDate);
        // 3. 把统计之后的数据，添加到统计结果表里面
        if (orderStatistics != null) {
            orderStatisticsMapper.insertStatistics(orderStatistics);
        }
    }

}
