package com.good.spzx.manager.service;

import com.github.pagehelper.PageInfo;
import com.good.spzx.model.dto.product.CategoryBrandDto;
import com.good.spzx.model.entity.product.Brand;
import com.good.spzx.model.entity.product.CategoryBrand;

import java.util.List;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/21 17:48
 */
public interface CategoryBrandService {

    // 条件分页查询 -
    PageInfo<CategoryBrand> getCategoryBrandPage(Integer current, Integer limit, CategoryBrandDto categoryBrandDto);

    // 添加
    void saveCategoryBrand(CategoryBrand categoryBrand);

    void updateCategoryBrand(CategoryBrand categoryBrand);

    void deleteCategoryBrand(Long id);

    // 根据分类id查询对应品牌信息
    List<Brand> findBrandByCategoryId(Long id);
}
