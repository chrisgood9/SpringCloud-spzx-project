package com.good.spzx.manager.service;

import com.github.pagehelper.PageInfo;
import com.good.spzx.model.entity.product.Brand;

import java.util.List;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/21 15:13
 */
public interface BrandService {
    // 获取品牌分页数据
    PageInfo<Brand> getBrandListByPage(Integer current, Integer limit);

    // 添加品牌数据
    void addBrand(Brand brand);

    // 更新品牌数据
    void updateBrand(Brand brand);

    // 根据id删除数据
    void deleteById(Integer id);

    // 查询所有品牌数据
    List<Brand> findAll();

}
