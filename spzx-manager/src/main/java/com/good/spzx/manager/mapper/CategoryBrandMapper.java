package com.good.spzx.manager.mapper;

import com.good.spzx.model.dto.product.CategoryBrandDto;
import com.good.spzx.model.entity.product.Brand;
import com.good.spzx.model.entity.product.CategoryBrand;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/21 17:48
 */
@Mapper
public interface CategoryBrandMapper {

    List<CategoryBrand> getCategoryBandPage(CategoryBrandDto categoryBrandDto);

    void saveCategoryBrand(CategoryBrand categoryBrand);

    void updateCategoryBrand(CategoryBrand categoryBrand);

    void deleteCategoryBrand(Long id);

    // 根据分类id查询对应品牌信息
    List<Brand> findBrandByCategoryId(Long id);
}
