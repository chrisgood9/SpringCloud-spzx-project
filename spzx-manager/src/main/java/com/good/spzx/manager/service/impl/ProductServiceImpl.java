package com.good.spzx.manager.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.good.spzx.manager.mapper.ProductDetailsMapper;
import com.good.spzx.manager.mapper.ProductMapper;
import com.good.spzx.manager.mapper.ProductSkuMapper;
import com.good.spzx.manager.service.ProductService;
import com.good.spzx.model.dto.product.ProductDto;
import com.good.spzx.model.entity.product.Product;
import com.good.spzx.model.entity.product.ProductDetails;
import com.good.spzx.model.entity.product.ProductSku;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/23 14:53
 */
@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductMapper productMapper;

    @Autowired
    private ProductDetailsMapper productDetailsMapper;

    @Autowired
    private ProductSkuMapper productSkuMapper;

    @Override
    public PageInfo<Product> findByPage(Integer page, Integer limit, ProductDto productDto) {
        PageHelper.startPage(page, limit);
        List<Product> productList = productMapper.findByPage(productDto);
        PageInfo<Product> pageInfo = new PageInfo<>(productList);
        return pageInfo;
    }

    @Override
    public void saveProduct(Product product) {
        //商品上架状态初始值是0 --- 审核状态：商品需要被审核通过后才能上架，初始值是0，等待审核
        product.setStatus(0);
        product.setAuditStatus(0);
        // 保存商品基本信息 - product表
        productMapper.saveProduct(product);

        // 保存商品sku列表集合 - 保存sku信息 product_sku表
        List<ProductSku> productSkuList = product.getProductSkuList();
        for (ProductSku productSku : productSkuList) {
            // 商品编号 - 这里改用uuid来保证唯一性
            productSku.setSkuCode(product.getId() + "-" + UUID.randomUUID().toString()
                    .replace("-", ""));
            // 商品id
            productSku.setProductId(product.getId());
            // skuName : 产品name+规格name
            productSku.setSkuName(product.getName() + productSku.getSkuSpec());
            productSku.setSaleNum(0);                               // 设置销量
            productSku.setStatus(0);
            productSkuMapper.saveProductSku(productSku);
        }

        // 保存商品详情数据 - product_details表
        ProductDetails productDetails = new ProductDetails();
        productDetails.setProductId(product.getId());
        productDetails.setImageUrls(product.getDetailsImageUrls());
        productDetailsMapper.saveDetails(productDetails);
    }

    @Override
    public Product getProductById(Long id) {
        // 获取商品基本信息
        Product product = productMapper.getProductById(id);
        // 获取商品sku信息
        List<ProductSku> productSkuList = productSkuMapper.getProductSkuById(product.getId());
        product.setProductSkuList(productSkuList);
        // 获取商品详情信息
        ProductDetails productDetails = productDetailsMapper.getDetailsById(product.getId());
        product.setDetailsImageUrls(productDetails.getImageUrls());
        return product;
    }

    @Override
    public void updateProductById(Product product) {
        // 修改商品基本信息
        productMapper.updateProductById(product);
        // 修改商品sku信息
        List<ProductSku> productSkuList = product.getProductSkuList();
        productSkuList.forEach(productSku -> {
            productSkuMapper.updateProductSkuById(productSku);
        });
        // 修改商品详情信息: 根据商品id查询信息，再重新设置 -> 调用更新方法
        String detailsImageUrls = product.getDetailsImageUrls();
        ProductDetails productDetails = productDetailsMapper.getDetailsById(product.getId());
        productDetails.setImageUrls(detailsImageUrls);
        productDetailsMapper.updateProductDetailsById(productDetails);
    }

    @Override
    public void deleteById(Long productId) {
        productMapper.deleteById(productId);
        productSkuMapper.deleteSkuById(productId);
        productDetailsMapper.deleteDetailsById(productId);
    }

    @Override
    public void updateAuditStatus(Long productId, Integer auditStatus) {
        Product product = new Product();
        product.setId(productId);
        if (auditStatus == 1) {
            product.setAuditStatus(1);
            product.setAuditMessage("审核通过");
        } else {
            product.setAuditStatus(-1);
            product.setAuditMessage("审核失败");
        }
        productMapper.updateProductById(product);
    }

    @Override
    public void updateStatus(Long productId, Integer status) {
        Product product = new Product();
        product.setId(productId);
        if (status == 1) {
            product.setStatus(1);
        } else {
            product.setStatus(-1);
        }
        productMapper.updateProductById(product);
    }
}
