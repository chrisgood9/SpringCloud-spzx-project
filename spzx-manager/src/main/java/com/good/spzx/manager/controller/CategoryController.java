package com.good.spzx.manager.controller;

import com.good.spzx.manager.service.CategoryService;
import com.good.spzx.model.entity.product.Category;
import com.good.spzx.model.vo.common.Result;
import com.good.spzx.model.vo.common.ResultCodeEnum;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/19 20:29
 */
@RestController
@RequestMapping("/admin/product/category")
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    /**
     * 分类列表，每次查询一层数据 (基于前端的懒加载)
     *
     * @param id
     * @return
     */
    @GetMapping(value = "/findByParentId/{id}")
    public Result findByParentId(@PathVariable("id") Long id) {
        List<Category> categoryList = categoryService.findByParentId(id);
        return Result.build(categoryList, ResultCodeEnum.SUCCESS);
    }

    @GetMapping(value = "/exportDataToExcel")
    public void exportDataToExcel(HttpServletResponse response) {
        categoryService.exportDataToExcel(response);
    }

    @GetMapping(value = "/importData")
    public void importData(MultipartFile file) {
        categoryService.importData(file);
    }
}

