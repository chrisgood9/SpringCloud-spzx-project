package com.good.spzx.manager.controller;

import com.github.pagehelper.PageInfo;
import com.good.spzx.common.log.annotation.Log;
import com.good.spzx.common.log.enums.OperatorType;
import com.good.spzx.manager.service.BrandService;
import com.good.spzx.model.entity.product.Brand;
import com.good.spzx.model.vo.common.Result;
import com.good.spzx.model.vo.common.ResultCodeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/21 15:11
 */
@RestController
@RequestMapping("/admin/product/brand")
public class BrandController {

    @Autowired
    private BrandService brandService;

    @Log(title = "品牌管理:列表", businessType = 0, operatorType = OperatorType.OTHER)
    @GetMapping(value = "/getBrandListByPage/{current}/{limit}")
    public Result getBrandListByPage(@PathVariable("current") Integer current,
                                     @PathVariable("limit") Integer limit) {
        PageInfo<Brand> brandPageInfo = brandService.getBrandListByPage(current, limit);
        return Result.build(brandPageInfo, ResultCodeEnum.SUCCESS);
    }

    @PostMapping(value = "/addBrand")
    public Result addBrand(@RequestBody Brand brand) {
        brandService.addBrand(brand);
        return Result.build(null, ResultCodeEnum.SUCCESS);
    }

    @PostMapping(value = "/updateBrand")
    public Result updateBrand(@RequestBody Brand brand) {
        brandService.updateBrand(brand);
        return Result.build(null, ResultCodeEnum.SUCCESS);
    }

    @DeleteMapping(value = "/deleteById/{id}")
    public Result deleteById(@PathVariable("id") Integer id) {
        brandService.deleteById(id);
        return Result.build(null, ResultCodeEnum.SUCCESS);
    }

    @GetMapping(value = "/findAll")
    public Result findAll() {
        List<Brand> brandList = brandService.findAll();
        return Result.build(brandList, ResultCodeEnum.SUCCESS);
    }

}
