package com.good.spzx.manager.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/2 15:36
 */
@Data
@ConfigurationProperties(prefix = "spzx.auth")
public class SysUserProperties {

    private List<String> noAuthUrls;
}
