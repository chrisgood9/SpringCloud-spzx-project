package com.good.spzx.manager.service.impl;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.read.listener.ReadListener;
import com.good.spzx.exception.MyException;
import com.good.spzx.manager.listener.ImportListener;
import com.good.spzx.manager.mapper.CategoryMapper;
import com.good.spzx.manager.service.CategoryService;
import com.good.spzx.model.entity.product.Category;
import com.good.spzx.model.vo.common.ResultCodeEnum;
import com.good.spzx.model.vo.product.CategoryExcelVo;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.EventListener;
import java.util.List;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/19 20:30
 */
@Service
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    private CategoryMapper categoryMapper;

    @Override
    public List<Category> findByParentId(Long id) {
        // 查询当前parentId的子节点集
        List<Category> categoryList = categoryMapper.selectCategoryByParentId(id);

        // hasChildren标记是否存在子节点 true:存在 false:不存在
        // 在数据库中没有该字段，自己封装
        if (!CollectionUtils.isEmpty(categoryList)) {
            categoryList.forEach(category -> {
                int count = categoryMapper.selectCountByParentId(category.getId());
                // 如果count > 0 有下一层 => hasChildren = true
                category.setHasChildren(count > 0);
            });
        }
        return categoryList;
    }

    /**
     * 导出数据到excel表格，通过下载的方式
     *
     * @param response
     */
    @Override
    public void exportDataToExcel(HttpServletResponse response) {
        try {
            // application/vnd.ms-excel 是 xls
            // 1. 设置响应头信息 xlsx
            response.setContentType("application/vnd.ms-excel");
            response.setCharacterEncoding("utf-8");
            // 2.文件名  这里URLEncoder.encode可以防止中文乱码 当然和easy excel没有关系
            String fileName = URLEncoder.encode("分类数据", "UTF-8");
            // 3. 设置请求头（以下载的方式导出，必须设置）
            response.setHeader("Content-disposition", "attachment; filename=" + fileName + ".xlsx");

            // 4. 查询数据库中的数据
            List<Category> categoryList = categoryMapper.selectAll();
            ArrayList<CategoryExcelVo> categoryExcelVos = new ArrayList<>(categoryList.size());
            // 转换格式 - 封装对象
            for (Category category : categoryList) {
                CategoryExcelVo vo = new CategoryExcelVo();
                BeanUtils.copyProperties(category, vo, CategoryExcelVo.class);
                categoryExcelVos.add(vo);
            }

            // 5. 写出数据到浏览器端
            EasyExcel.write(response.getOutputStream(), CategoryExcelVo.class)
                    .sheet("分类数据").doWrite(categoryExcelVos);
        } catch (IOException e) {
            e.printStackTrace();
            new MyException(ResultCodeEnum.DATA_ERROR);
        }
    }

    /**
     * 导入数据
     *
     * @param file
     */
    @Override
    public void importData(MultipartFile file) {
        try {
            // 1. 获取监听器
            ImportListener<CategoryExcelVo> eventListener = new ImportListener<>(categoryMapper);
            // 2. 导入数据
            EasyExcel.read(file.getInputStream(), CategoryExcelVo.class, eventListener)
                    .sheet().doRead();
        } catch (IOException e) {
            e.printStackTrace();
            throw new MyException(ResultCodeEnum.DATA_ERROR);
        }

    }


}
