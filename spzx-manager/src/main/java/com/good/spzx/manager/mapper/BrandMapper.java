package com.good.spzx.manager.mapper;

import com.good.spzx.model.entity.product.Brand;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/21 15:14
 */
@Mapper
public interface BrandMapper {

    // 获取分页数据
    List<Brand> getBrandPage();

    // 添加品牌数据
    void saveBrand(Brand brand);

    // 更新品牌数据
    void updateBrand(Brand brand);

    // 根据id删除数据
    void deleteById(Integer id);
}
