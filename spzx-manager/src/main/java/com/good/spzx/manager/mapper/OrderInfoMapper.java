package com.good.spzx.manager.mapper;

import com.good.spzx.model.entity.order.OrderStatistics;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/24 16:47
 */
@Mapper
public interface OrderInfoMapper {
    // 根据日期统计交易金额
    OrderStatistics selectOrderStatisticsByDate(String createDate);
}
