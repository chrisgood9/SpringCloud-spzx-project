package com.good.spzx.manager.listener;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.read.listener.ReadListener;
import com.alibaba.excel.util.ListUtils;
import com.alibaba.fastjson.JSON;
import com.good.spzx.manager.mapper.CategoryMapper;
import com.good.spzx.model.entity.product.Category;
import com.good.spzx.model.vo.product.CategoryExcelVo;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/21 13:21
 */
@Slf4j
public class ImportListener<T> implements ReadListener<T> {
    /**
     * 每隔5条存储数据库，实际使用中可以100条，然后清理list ，方便内存回收
     */
    private static final int BATCH_COUNT = 100;

    /**
     * 缓存的数据
     */
    private List<T> categoryList = ListUtils.newArrayListWithExpectedSize(BATCH_COUNT);

    private CategoryMapper categoryMapper;

    public ImportListener(CategoryMapper categoryMapper) {
        this.categoryMapper = categoryMapper;
    }

    /**
     * 这个每一条数据解析都会来调用
     *
     * @param t
     * @param analysisContext
     */
    @Override
    public void invoke(T t, AnalysisContext analysisContext) {
        log.info("解析到一条数据:{}", JSON.toJSONString(t));
        categoryList.add(t);
        if (categoryList.size() >= BATCH_COUNT) {
            // 保存数据
            saveData();
            // 存储完成清理 list
            categoryList = ListUtils.newArrayListWithExpectedSize(BATCH_COUNT);
        }
    }

    /**
     * 所有数据解析完成了 都会来调用
     *
     * @param analysisContext
     */
    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {
        // 这里也要保存数据，确保最后遗留的数据也存储到数据库
        saveData();
        log.info("所有数据解析完成！");
    }

    private void saveData() {
        categoryMapper.batchInsert((List<CategoryExcelVo>) categoryList);
    }
}
