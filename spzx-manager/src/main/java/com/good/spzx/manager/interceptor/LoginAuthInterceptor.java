package com.good.spzx.manager.interceptor;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.util.StringUtil;
import com.good.spzx.model.entity.system.SysUser;
import com.good.spzx.model.vo.common.Result;
import com.good.spzx.model.vo.common.ResultCodeEnum;
import com.good.spzx.utils.AuthContextUtil;
import io.micrometer.common.util.StringUtils;
import jakarta.annotation.Resource;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.concurrent.TimeUnit;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/2 14:48
 */

/**
 * 登录拦截器
 */
@Component
public class LoginAuthInterceptor implements HandlerInterceptor {

    @Resource
    private RedisTemplate<String, String> redisTemplate;

    /**
     * 控制器方法前执行 (return true -> 放行)
     * 1. 如果是跨域预检请求（OPTIONS），直接放行
     * 2. 请求头中获取token值，
     * 3. 查询redis中是否有token数据，
     * 4. 存在redis中的信息-存入threadLocal线程变量中
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //1. 获取请求方式，如果是跨域预检请求，直接放行
        String method = request.getMethod();
        if ("OPTIONS".equals(method)) {
            return true;
        }
        //2. 判断请求头的token是否存在
        String token = request.getHeader("token");
        if (StringUtil.isEmpty(token)) {
            responseNoLoginInfo(response);//返回错误信息
            return false;
        }
        //3. token不为空，进一步验证redis中是否存在用户信息
        String redisInfoForJson = redisTemplate.opsForValue().get("user:login" + token);
        if (StringUtil.isEmpty(redisInfoForJson)) {
            responseNoLoginInfo(response);
            return false;
        }
        //4. redis存在登录的用户信息，存入ThreadLocal线程变量中
        SysUser sysUser = JSON.parseObject(redisInfoForJson, SysUser.class);
        AuthContextUtil.set(sysUser);
        //5. 重置redis中的用户数据的有效时间ttl 30min
        redisTemplate.expire("user:login" + token, 30, TimeUnit.MINUTES);
        return true;
    }

    /**
     * 控制器方法执行后执行
     */
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

    }

    /**
     * 全部执行后执行
     */
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

    }

    //响应208状态码给前端
    private void responseNoLoginInfo(HttpServletResponse response) {
        Result<Object> result = Result.build(null, ResultCodeEnum.LOGIN_AUTH);
        PrintWriter writer = null;
        response.setCharacterEncoding("UTF-8");//设置服务器端编码
        response.setContentType("text/html; charset=utf-8");//设置浏览器端编码
        try {
            writer = response.getWriter(); //获取打印输出流
            writer.print(JSON.toJSONString(result)); //通过响应给客户端以Json格式
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (writer != null) writer.close();
        }

    }
}
