package com.good.spzx.manager.mapper;

import com.good.spzx.model.entity.system.SysMenu;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/18 20:17
 */
@Mapper
public interface SysMenuMapper {

    List<SysMenu> selectAll();

    void addMenu(SysMenu sysMenu);

    void update(SysMenu sysMenu);

    void delete(Long menuId);

    // 查询子节点的个数
    Integer selectChildren(Long menuId);

    List<SysMenu> findUserMenuList(Long userId);

    // 获取父菜单
    SysMenu selectParentMenu(Long parentId);
}
