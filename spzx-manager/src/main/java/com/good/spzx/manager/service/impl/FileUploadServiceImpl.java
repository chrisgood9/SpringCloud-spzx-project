package com.good.spzx.manager.service.impl;

import cn.hutool.core.date.DateUtil;
import com.good.spzx.exception.MyException;
import com.good.spzx.manager.properties.MinioProperties;
import com.good.spzx.manager.service.FileUploadService;
import com.good.spzx.model.vo.common.ResultCodeEnum;
import io.minio.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;
import java.util.UUID;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/18 10:31
 */
@Service
public class FileUploadServiceImpl implements FileUploadService {

    @Autowired
    private MinioProperties minioProperties;

    @Override
    public String uploadFile(MultipartFile file) {
        try {
            // 创建一个Minio的客户端对象
            MinioClient minioClient =
                    MinioClient.builder()
                            .endpoint(minioProperties.getEndpointUrl())
                            .credentials(minioProperties.getAccessKey(), minioProperties.getSecreKey())
                            .build();
            // 判断是否存在
            boolean found =
                    minioClient.bucketExists(BucketExistsArgs.builder().bucket(minioProperties.getBucketName()).build());
            // 如果不存在，那么此时就创建一个新的桶
            if (!found) {
                // Make a new bucket called 'asiatrip'.
                minioClient.makeBucket(MakeBucketArgs.builder().bucket(minioProperties.getBucketName()).build());
            } else {
                System.out.println("Bucket 'asiatrip' already exists.");
            }

            // 获取文件名
            // 文件名格式：日期/uuid文件初始名.jpg
            // 设置存储对象名称
            String dateDir = DateUtil.format(new Date(), "yyyyMMdd");
            String uuid = UUID.randomUUID().toString().replace("-", "");
            String fileName = dateDir + "/" + uuid + file.getOriginalFilename();

            // 上传文件 ：用stream传输
            PutObjectArgs putObjectArgs = PutObjectArgs.builder()
                    .bucket(minioProperties.getBucketName())
                    .stream(file.getInputStream(), file.getSize(), -1)
                    .object(fileName)
                    .build();
            minioClient.putObject(putObjectArgs);

            // 获取Url
            // http://192.168.116.133:9001/spzx-bucket/文件名
            String url = minioProperties.getEndpointUrl() + "/" + minioProperties.getBucketName() + "/" + fileName;
            System.out.println(url);
            return url;
        } catch (Exception e) {
            System.out.println("Error occurred: " + e);
            throw new MyException(ResultCodeEnum.SYSTEM_ERROR);
        }
    }
}
