package com.good.spzx.manager.mapper;

import com.good.spzx.model.entity.product.ProductDetails;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/23 15:15
 */
@Mapper
public interface ProductDetailsMapper {
    // 保存商品详情
    void saveDetails(ProductDetails productDetails);

    // 根据商品id获取商品详情信息
    ProductDetails getDetailsById(Long id);

    // 修改商品详情信息
    void updateProductDetailsById(ProductDetails productDetails);

    // 根据商品id删除商品详情数据
    void deleteDetailsById(Long productId);
}
