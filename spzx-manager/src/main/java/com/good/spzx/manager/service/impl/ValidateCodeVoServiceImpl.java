package com.good.spzx.manager.service.impl;

import cn.hutool.captcha.CaptchaUtil;
import cn.hutool.captcha.CircleCaptcha;
import com.good.spzx.manager.service.ValidateCodeVoService;
import com.good.spzx.model.vo.system.ValidateCodeVo;
import jakarta.annotation.Resource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/1 16:15
 */
@Service
public class ValidateCodeVoServiceImpl implements ValidateCodeVoService {

    @Resource
    private RedisTemplate<String, String> redisTemplate;

    //获取验证码图片
    @Override
    public ValidateCodeVo generateValidateCode() {
        //1. 用工具生成验证码 —— 使用hutool工具包中的工具类生成图片验证码
        //  参数：宽  高  验证码位数 干扰线数量
        CircleCaptcha circleCaptcha = CaptchaUtil.createCircleCaptcha(150, 48, 4, 10);
        String codeValue = circleCaptcha.getCode();
        String imageBase64 = circleCaptcha.getImageBase64();//获取验证码图片，用base64进行编码
        //2. 保存到redis中
        String key = UUID.randomUUID().toString().replace("-", "");
        redisTemplate.opsForValue().set("user:validatecode" + key
                , codeValue, 5, TimeUnit.MINUTES);
        //3. 返回对象
        ValidateCodeVo validateCodeVo = new ValidateCodeVo();
        validateCodeVo.setCodeKey(key);
        validateCodeVo.setCodeValue("data:image/png;base64," + imageBase64);
        return validateCodeVo;
    }
}
