package com.good.spzx.manager.mapper;

import com.good.spzx.model.entity.product.ProductSku;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/23 15:18
 */
@Mapper
public interface ProductSkuMapper {

    // 保存商品Sku信息
    void saveProductSku(ProductSku productSku);

    // 根据商品id获取商品Sku列表数据
    List<ProductSku> getProductSkuById(Long id);

    // 修改商品sku信息
    void updateProductSkuById(ProductSku productSku);

    // 根据商品id删除sku数据
    void deleteSkuById(Long productId);
}
