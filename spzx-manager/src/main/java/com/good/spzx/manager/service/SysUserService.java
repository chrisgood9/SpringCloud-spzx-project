package com.good.spzx.manager.service;

import com.github.pagehelper.PageInfo;
import com.good.spzx.model.dto.system.AssignRoleDto;
import com.good.spzx.model.dto.system.LoginDto;
import com.good.spzx.model.dto.system.SysUserDto;
import com.good.spzx.model.entity.system.SysUser;
import com.good.spzx.model.vo.system.LoginVo;
import com.good.spzx.model.vo.system.SysMenuVo;

import java.util.List;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/10/31 16:09
 */
public interface SysUserService {

    // 登录
    LoginVo login(LoginDto loginDto);

    // 获取用户权证信息
    SysUser getUserInfo(String token);

    // 用户退出
    void logout(String token);

    // 获取用户列表
    PageInfo<SysUser> getByPage(Integer current, Integer limit, SysUserDto sysUserDto);

    // 添加用户
    void saveSysUser(SysUser sysUser);

    // 修改用户
    void update(SysUser sysUser);

    // 删除用户
    void deleteById(Integer id);

    // 给用户分配角色
    void doAssign(AssignRoleDto assignRoleDto);

}
