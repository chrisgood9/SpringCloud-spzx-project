package com.good.spzx.manager.service.impl;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.util.StringUtil;
import com.good.spzx.exception.MyException;
import com.good.spzx.manager.mapper.SysUserMapper;
import com.good.spzx.manager.mapper.SysUserRoleMapper;
import com.good.spzx.manager.service.SysUserService;
import com.good.spzx.model.dto.system.AssignRoleDto;
import com.good.spzx.model.dto.system.LoginDto;
import com.good.spzx.model.dto.system.SysUserDto;
import com.good.spzx.model.entity.system.SysRole;
import com.good.spzx.model.entity.system.SysUser;
import com.good.spzx.model.vo.common.ResultCodeEnum;
import com.good.spzx.model.vo.system.LoginVo;
import com.good.spzx.model.vo.system.SysMenuVo;
import com.good.spzx.utils.AuthContextUtil;
import jakarta.annotation.Resource;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.DigestUtils;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/10/31 16:10
 */
@Service
public class SysUserServiceImpl implements SysUserService {

    //可以声明一个类，专门放这些常量
    public static final String VALIDATE_CODE_KEY_PREFIX = "user:validatecode";

    @Autowired
    private SysUserMapper sysUsrMapper;

    @Autowired
    private SysUserRoleMapper sysUserRoleMapper;

    @Resource
    private RedisTemplate<String, String> redisTemplate;

    @Override
    public LoginVo login(LoginDto loginDto) {
        /** 验证 验证码
         *  1. 获取输入的验证码和存储到redis的key名称
         *  2. 获取redis中的验证码的值
         *  3. 输入验证码和redis中验证码比较
         *  4. 如果不一致，校验失败
         *  5. 如果一致，删除redis中的验证码，因为已经没有用了
         */
        String captcha = loginDto.getCaptcha();
        String codeKey = loginDto.getCodeKey();
        String redisCode = redisTemplate.opsForValue().get(VALIDATE_CODE_KEY_PREFIX + codeKey);
        if (StringUtil.isEmpty(redisCode) || !StringUtils.equalsIgnoreCase(redisCode, captcha)) {
            throw new MyException(ResultCodeEnum.VALIDATECODE_ERROR);
        }
        redisTemplate.delete(VALIDATE_CODE_KEY_PREFIX + codeKey);

        /**
         * 验证用户名和密码
         */
        //1 获取提交的用户名
        String userName = loginDto.getUserName();
        //2 根据用户名查询数据表 sys_user表
        SysUser sysUser = sysUsrMapper.selectUserInfoByUserName(userName);
        //3 如果根据用户名查询不到对应信息，用户不存在，返回错误信息
        if (userName.isEmpty()) {
//            throw new RuntimeException("用户名不存在");
            throw new MyException(ResultCodeEnum.LOGIN_ERROR);
        }
        //4 如果查询到信息，用户存在
        //5 获取输入的密码，比较输入的密码和数据库是否一致
        //数据库中的密码是加密后的密码，用md5加密
        String databasePassword = sysUser.getPassword();
        String inputPassword = DigestUtils.md5DigestAsHex(loginDto.getPassword().getBytes());
        if (!databasePassword.equals(inputPassword)) {
            throw new RuntimeException("密码不正确");
        }
        //6 如果密码一致，登录成功，否则登录失败
        //7 登陆成功，生成用户唯一标识 token
        String token = UUID.randomUUID().toString().replaceAll("-", "");
        //8 把登录成功的用户信息放到redis里边，并设计过期时间
        //JSON.toJSONSting() : 转成json格式
        redisTemplate.opsForValue().
                set("user:login" + token, JSON.toJSONString(sysUser),
                        7, TimeUnit.DAYS);
        //9 返回 loginVo 对象
        LoginVo loginVo = new LoginVo();
        loginVo.setToken(token);
        return loginVo;
    }

    // 在redis中获取用户信息，
    @Override
    public SysUser getUserInfo(String token) {
        String sysUserJson = redisTemplate.opsForValue().get("user:login" + token);
        SysUser sysUser = JSON.parseObject(sysUserJson, SysUser.class);
        return sysUser;
    }

    /**
     * 用户退出
     * 只需要在redis中删除用户信息即可
     */
    @Override
    public void logout(String token) {
        redisTemplate.delete("user:login" + token);
    }


    // 获取用户列表信息
    @Override
    public PageInfo<SysUser> getByPage(Integer current, Integer limit, SysUserDto sysUserDto) {
        PageHelper.startPage(current, limit);
        List<SysUser> pageList = sysUsrMapper.getByPage(sysUserDto);
        PageInfo<SysUser> pageInfo = new PageInfo<>(pageList);
        return pageInfo;
    }

    @Override
    public void saveSysUser(SysUser sysUser) {
        String userName = sysUser.getUserName();
        SysUser dbSysUser = sysUsrMapper.selectUserInfoByUserName(userName);
        // 判断是否已经存在用户
        if (dbSysUser != null) {
            throw new MyException(ResultCodeEnum.USER_NAME_IS_EXISTS);
        }
        // 密码加密
        String password = DigestUtils.md5DigestAsHex(sysUser.getPassword().getBytes());
        sysUser.setPassword(password);
        sysUsrMapper.save(sysUser);
    }

    @Override
    public void update(SysUser sysUser) {
        sysUsrMapper.update(sysUser);
    }

    @Override
    public void deleteById(Integer id) {
        sysUsrMapper.deleteById(id);
    }

    @Override
    @Transactional
    public void doAssign(AssignRoleDto assignRoleDto) {
        // TODO 两个操作应当是原子的，这里未涉及事务
        // 1.删除当前用户分配的角色
        Long userId = assignRoleDto.getUserId();
        sysUserRoleMapper.deletedById(userId);
        // 2.重新分配角色
        List<Long> roleIdList = assignRoleDto.getRoleIdList();
        for (Long roleId : roleIdList) {
            sysUserRoleMapper.assign(userId, roleId);
        }
    }

}
