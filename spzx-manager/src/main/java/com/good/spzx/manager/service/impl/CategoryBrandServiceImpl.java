package com.good.spzx.manager.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.good.spzx.manager.mapper.CategoryBrandMapper;
import com.good.spzx.manager.service.CategoryBrandService;
import com.good.spzx.model.dto.product.CategoryBrandDto;
import com.good.spzx.model.entity.product.Brand;
import com.good.spzx.model.entity.product.CategoryBrand;
import com.good.spzx.model.vo.product.CategoryExcelVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/21 17:48
 */
@Service
public class CategoryBrandServiceImpl implements CategoryBrandService {

    @Autowired
    private CategoryBrandMapper categoryBrandMapper;

    /**
     * 分页品牌 - 条件分页查询
     */
    @Override
    public PageInfo<CategoryBrand> getCategoryBrandPage(Integer current, Integer limit, CategoryBrandDto categoryBrandDto) {
        PageHelper.startPage(current, limit);
        List<CategoryBrand> brandList = categoryBrandMapper.getCategoryBandPage(categoryBrandDto);
        PageInfo<CategoryBrand> pageInfo = new PageInfo<>(brandList);
        return pageInfo;
    }

    /**
     * 添加
     *
     * @param categoryBrand
     */
    @Override
    public void saveCategoryBrand(CategoryBrand categoryBrand) {
        categoryBrandMapper.saveCategoryBrand(categoryBrand);
    }

    @Override
    public void updateCategoryBrand(CategoryBrand categoryBrand) {
        categoryBrandMapper.updateCategoryBrand(categoryBrand);
    }

    @Override
    public void deleteCategoryBrand(Long id) {
        categoryBrandMapper.deleteCategoryBrand(id);
    }


    @Override
    public List<Brand> findBrandByCategoryId(Long id) {
        return categoryBrandMapper.findBrandByCategoryId(id);
    }
}
