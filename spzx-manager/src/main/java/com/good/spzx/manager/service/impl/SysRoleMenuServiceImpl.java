package com.good.spzx.manager.service.impl;

import com.good.spzx.manager.mapper.SysRoleMenuMapper;
import com.good.spzx.manager.service.SysMenuService;
import com.good.spzx.manager.service.SysRoleMenuService;
import com.good.spzx.model.dto.system.AssginMenuDto;
import com.good.spzx.model.entity.system.SysMenu;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/19 9:52
 */
@Service
public class SysRoleMenuServiceImpl implements SysRoleMenuService {

    @Autowired
    private SysRoleMenuMapper sysRoleMenuMapper;

    @Autowired
    private SysMenuService sysMenuService;

    @Override
    public Map<String, Object> findAllRoleMenuById(Long roleId) {
        // 所有菜单列表
        List<SysMenu> menuList = sysMenuService.findNodes();
        List<SysMenu> roleMenuIds = sysRoleMenuMapper.findRoleMenuIds(roleId);
        HashMap<String, Object> map = new HashMap<>();
        map.put("menuList", menuList);
        map.put("roleMenuIds", roleMenuIds);
        return map;
    }

    @Override
    public void doAssign(AssginMenuDto assginMenuDto) {
        // 删除所选
        sysRoleMenuMapper.deletedById(assginMenuDto.getRoleId());
        // 添加当前选中的
        List<Map<String, Number>> menuIdList = assginMenuDto.getMenuIdList();
        if (menuIdList != null && menuIdList.size() > 0) {
            sysRoleMenuMapper.doAssign(assginMenuDto);
        }
    }
}
