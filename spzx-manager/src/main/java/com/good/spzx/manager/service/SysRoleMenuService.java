package com.good.spzx.manager.service;

import com.good.spzx.model.dto.system.AssginMenuDto;

import java.util.Map;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/19 9:52
 */
public interface SysRoleMenuService {

    Map<String, Object> findAllRoleMenuById(Long roleId);

    void doAssign(AssginMenuDto assginMenuDto);
}
