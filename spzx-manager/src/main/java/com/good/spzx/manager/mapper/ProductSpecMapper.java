package com.good.spzx.manager.mapper;

import com.good.spzx.model.entity.product.ProductSpec;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/22 14:22
 */
@Mapper
public interface ProductSpecMapper {

    // 条件分页查询 - 所有商品规格信息
    List<ProductSpec> findAll();

    void saveSpec(ProductSpec productSpec);

    void updateSpec(ProductSpec productSpec);

    void deleteSpec(Long id);

}

