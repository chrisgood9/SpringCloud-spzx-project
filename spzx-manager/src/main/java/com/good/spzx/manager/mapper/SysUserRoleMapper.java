package com.good.spzx.manager.mapper;

import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/18 13:55
 */
@Mapper
public interface SysUserRoleMapper {

    // 根据用户id删除
    void deletedById(Long userId);

    // 给用户分配角色
    void assign(Long userId, Long roleId);

    // 根据id获取所具有的角色id
    List<Long> getRoleListByUserId(Long userId);
}
