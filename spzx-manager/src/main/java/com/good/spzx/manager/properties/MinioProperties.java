package com.good.spzx.manager.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/18 11:56
 */
@Data
@ConfigurationProperties(prefix = "spzx.minio")
public class MinioProperties {

    private String endpointUrl;

    private String bucketName;

    private String accessKey;

    private String secreKey;

}
