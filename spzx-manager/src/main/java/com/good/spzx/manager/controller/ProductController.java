package com.good.spzx.manager.controller;

import com.github.pagehelper.PageInfo;
import com.good.spzx.manager.service.ProductService;
import com.good.spzx.model.dto.product.ProductDto;
import com.good.spzx.model.entity.product.Product;
import com.good.spzx.model.vo.common.Result;
import com.good.spzx.model.vo.common.ResultCodeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/23 14:52
 */
@RestController
@RequestMapping("/admin/product/product")
public class ProductController {

    @Autowired
    private ProductService productService;

    /**
     * 条件分页查询 - 所有商品信息
     *
     * @param page
     * @param limit
     * @param productDto
     * @return
     */
    @GetMapping(value = "/{page}/{limit}")
    public Result findByPage(@PathVariable("page") Integer page,
                             @PathVariable("limit") Integer limit,
                             ProductDto productDto) {
        PageInfo<Product> pageInfo = productService.findByPage(page, limit, productDto);
        return Result.build(pageInfo, ResultCodeEnum.SUCCESS);
    }

    /**
     * 保存商品数据
     *
     * @param product
     * @return
     */
    @PostMapping(value = "/saveProduct")
    public Result saveProduct(@RequestBody Product product) {
        productService.saveProduct(product);
        return Result.build(null, ResultCodeEnum.SUCCESS);
    }

    /**
     * 根据商品id获取商品信息
     *
     * @param id
     * @return
     */
    @GetMapping(value = "/getProduct/{id}")
    public Result getProduct(@PathVariable("id") Long id) {
        Product product = productService.getProductById(id);
        return Result.build(product, ResultCodeEnum.SUCCESS);
    }

    /**
     * 根据商品id修改商品信息
     *
     * @param product
     * @return
     */
    @PutMapping(value = "/updateProductById")
    public Result updateProductById(@RequestBody Product product) {
        productService.updateProductById(product);
        return Result.build(null, ResultCodeEnum.SUCCESS);
    }

    /**
     * 根据商品id删除商品信息
     *
     * @param productId
     * @return
     */
    @DeleteMapping(value = "/deleteById/{productId}")
    public Result deleteById(@PathVariable("productId") Long productId) {
        productService.deleteById(productId);
        return Result.build(null, ResultCodeEnum.SUCCESS);
    }

    /**
     * 商品审核 (审核状态：0-初始值，1-通过，-1-未通过)
     *
     * @param productId
     * @param auditStatus
     * @return
     */
    @PutMapping(value = "/updateAuditStatus/{productId}/{auditStatus}")
    public Result updateAuditStatus(@PathVariable("productId") Long productId,
                                    @PathVariable("auditStatus") Integer auditStatus) {
        productService.updateAuditStatus(productId, auditStatus);
        return Result.build(null, ResultCodeEnum.SUCCESS);
    }

    /**
     * 商品上下架 (线上状态：0-初始值，1-上架，-1-自主下架)
     *
     * @param productId
     * @param status
     * @return
     */
    @PutMapping(value = "updateStatus/{productId}/{status}")
    public Result updateStatus(@PathVariable("productId") Long productId,
                               @PathVariable("status") Integer status) {
        productService.updateStatus(productId, status);
        return Result.build(null, ResultCodeEnum.SUCCESS);
    }
}
