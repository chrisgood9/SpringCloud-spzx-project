package com.good.spzx.manager.service.impl;

import com.alibaba.excel.util.DateUtils;
import com.good.spzx.manager.mapper.OrderStatisticsMapper;
import com.good.spzx.manager.service.OrderInfoService;
import com.good.spzx.model.dto.order.OrderStatisticsDto;
import com.good.spzx.model.entity.order.OrderStatistics;
import com.good.spzx.model.vo.order.OrderStatisticsVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/24 17:20
 */
@Service
public class OrderInfoServiceImpl implements OrderInfoService {

    @Autowired
    private OrderStatisticsMapper orderStatisticsMapper;

    @Override
    public OrderStatisticsVo getOrderStaticsData(OrderStatisticsDto orderStatisticsDto) {
        // 根据dto条件 查询统计结果数据，返回list集合
        List<OrderStatistics> statisticsList = orderStatisticsMapper.getStatisticsForDate(orderStatisticsDto);
        // 遍历list获取 - 日期集合,并生成dataList集合
        List<String> dateList = statisticsList.stream().map(orderStatistics ->
                DateUtils.format(orderStatistics.getOrderDate(), "yyyy-MM-dd"))
                .collect(Collectors.toList());
        // 遍历list获取 - 所有日期对应的金额集合,并生成decimalList集合
        List<BigDecimal> amountList = statisticsList.stream()
                .map(OrderStatistics::getTotalAmount)
                .collect(Collectors.toList());

        OrderStatisticsVo orderStatisticsVo = new OrderStatisticsVo();
        orderStatisticsVo.setDateList(dateList);
        orderStatisticsVo.setAmountList(amountList);
        return orderStatisticsVo;
    }
}
