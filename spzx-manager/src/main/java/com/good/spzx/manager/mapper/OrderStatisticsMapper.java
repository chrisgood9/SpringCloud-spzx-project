package com.good.spzx.manager.mapper;

import com.good.spzx.model.dto.order.OrderStatisticsDto;
import com.good.spzx.model.entity.order.OrderStatistics;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/24 16:47
 */
@Mapper
public interface OrderStatisticsMapper {
    // 添加统计结果信息
    void insertStatistics(OrderStatistics orderStatistics);

    // 根据日期范围内查询统计结果数据，返回list集合
    List<OrderStatistics> getStatisticsForDate(OrderStatisticsDto orderStatisticsDto);
}
