package com.good.spzx.manager.service;

import org.springframework.web.multipart.MultipartFile;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/18 10:31
 */
public interface FileUploadService {

    // 头像上传
    String uploadFile(MultipartFile file);
}
