package com.good.spzx.manager.service;

import com.github.pagehelper.PageInfo;
import com.good.spzx.model.dto.system.SysRoleDto;
import com.good.spzx.model.entity.system.SysRole;

import java.util.Map;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/16 13:41
 */
public interface SysRoleService {

    // 获取角色列表
    PageInfo<SysRole> findByPage(SysRoleDto sysRoleDto, Integer current, Integer limit);

    // 添加角色
    void saveSysRole(SysRole sysRole);

    // 修改角色
    void updateSysRole(SysRole sysRole);

    // 删除角色
    void deleteById(Integer roleId);

    // 查询所有角色
    Map<String, Object> findAllRoles(Long userId);
}
