package com.good.spzx.manager;

import com.good.spzx.common.log.annotation.EnableLogAspect;
import com.good.spzx.manager.properties.MinioProperties;
import com.good.spzx.manager.properties.SysUserProperties;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/10/30 23:31
 */
@SpringBootApplication(scanBasePackages = "com.good.spzx")
@MapperScan("com/good/spzx/manager/mapper")
@EnableConfigurationProperties(value = {SysUserProperties.class, MinioProperties.class})
@EnableScheduling // 开启定时任务Spring Task
@EnableLogAspect
public class ManagerApplication {
    public static void main(String[] args) {
        SpringApplication.run(ManagerApplication.class, args);
    }
}
