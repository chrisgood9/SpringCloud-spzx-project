package com.good.spzx.manager.controller;

import com.github.pagehelper.PageInfo;
import com.good.spzx.manager.service.SysMenuService;
import com.good.spzx.manager.service.SysUserService;
import com.good.spzx.model.dto.system.AssignRoleDto;
import com.good.spzx.model.dto.system.SysUserDto;
import com.good.spzx.model.entity.system.SysUser;
import com.good.spzx.model.vo.common.Result;
import com.good.spzx.model.vo.common.ResultCodeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/17 9:13
 */
@RestController
@RequestMapping("/admin/system/sysUser")
public class SysUserController {

    @Autowired
    private SysUserService sysUserService;

    /**
     * 获取用户信息列表
     *
     * @param current
     * @param limit
     * @param sysUserDto
     * @return
     */
    @GetMapping(value = "/getByPage/{current}/{limit}")
    public Result getByPage(@PathVariable("current") Integer current,
                            @PathVariable("limit") Integer limit,
                            SysUserDto sysUserDto) {
        PageInfo<SysUser> pageInfo = sysUserService.getByPage(current, limit, sysUserDto);
        return Result.build(pageInfo, ResultCodeEnum.SUCCESS);
    }

    /**
     * 添加用户
     *
     * @param sysUser
     * @return
     */
    @PostMapping(value = "/saveSysUser")
    public Result saveSysUser(SysUser sysUser) {
        sysUserService.saveSysUser(sysUser);
        return Result.build(null, ResultCodeEnum.SUCCESS);
    }

    /**
     * 更新用户
     *
     * @param sysUser
     * @return
     */
    @PostMapping(value = "/update")
    public Result update(@RequestBody SysUser sysUser) {
        sysUserService.update(sysUser);
        return Result.build(null, ResultCodeEnum.SUCCESS);
    }

    /**
     * 删除用户
     *
     * @param id
     * @return
     */
    @DeleteMapping(value = "/delete/{id}")
    public Result deleteById(@PathVariable("id") Integer id) {
        sysUserService.deleteById(id);
        return Result.build(null, ResultCodeEnum.SUCCESS);
    }

    /**
     * 给用户分配角色
     *
     * @param assignRoleDto
     * @return
     */
    @PostMapping(value = "/doAssign")
    public Result doAssign(@RequestBody AssignRoleDto assignRoleDto) {
        sysUserService.doAssign(assignRoleDto);
        return Result.build(null, ResultCodeEnum.SUCCESS);
    }
}
