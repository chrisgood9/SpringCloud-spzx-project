package com.good.spzx.manager.mapper;

import com.good.spzx.model.entity.product.Category;
import com.good.spzx.model.vo.product.CategoryExcelVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/19 20:31
 */
@Mapper
public interface CategoryMapper {

    // 查询当前parentId下的子节点集
    List<Category> selectCategoryByParentId(Long id);

    // 查询当前 id 的子节点的个数
    int selectCountByParentId(Long id);

    // 查询表中所有数据
    List<Category> selectAll();

    // 批量数据插入
    void batchInsert(List<CategoryExcelVo> categoryList);
}
