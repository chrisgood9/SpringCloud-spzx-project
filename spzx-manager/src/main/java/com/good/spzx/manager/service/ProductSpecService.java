package com.good.spzx.manager.service;

import com.github.pagehelper.PageInfo;
import com.good.spzx.model.entity.product.ProductSpec;

import java.util.List;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/22 14:22
 */
public interface ProductSpecService {

    PageInfo<ProductSpec> findByPage(Integer current, Integer limit, ProductSpec productSpec);

    void saveSpec(ProductSpec productSpec);

    void updateSpec(ProductSpec productSpec);

    void deleteSpec(Long id);

    List<ProductSpec> findAll();

}
