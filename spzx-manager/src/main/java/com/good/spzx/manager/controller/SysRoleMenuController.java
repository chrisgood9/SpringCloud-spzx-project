package com.good.spzx.manager.controller;

import com.good.spzx.manager.service.SysRoleMenuService;
import com.good.spzx.model.dto.system.AssginMenuDto;
import com.good.spzx.model.vo.common.Result;
import com.good.spzx.model.vo.common.ResultCodeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/19 9:54
 */
@RestController
@RequestMapping(value = "/admin/system/sysRoleMenu")
public class SysRoleMenuController {

    @Autowired
    private SysRoleMenuService sysRoleMenuService;

    /**
     * 查询所有菜单列表 和 当前角色已经分配的菜单id列表
     *
     * @param roleId
     * @return
     */
    @GetMapping(value = "/findSysRoleMenuByRoleId/{roleId}")
    public Result findAllRoleMenuById(@PathVariable("roleId") Long roleId) {
        Map<String, Object> map = sysRoleMenuService.findAllRoleMenuById(roleId);
        return Result.build(map, ResultCodeEnum.SUCCESS);
    }

    /**
     * 添加
     * @param assginMenuDto
     * @return
     */
    @PostMapping(value = "/doAssign")
    public Result doAssign(@RequestBody AssginMenuDto assginMenuDto) {
        sysRoleMenuService.doAssign(assginMenuDto);
        return Result.build(null, ResultCodeEnum.SUCCESS);
    }

}
