package com.good.spzx.manager.service.impl;

import com.good.spzx.common.log.service.AsyncOperLogService;
import com.good.spzx.manager.mapper.SysOperLogMapper;
import com.good.spzx.model.entity.system.SysOperLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/24 21:41
 */
@Service
public class AsyncOperLogServiceImpl implements AsyncOperLogService {

    @Autowired
    private SysOperLogMapper sysOperLogMapper;

    @Async // 异步执行保存日志操作
    @Override
    public void saveSysOperLog(SysOperLog sysOperLog) {
        sysOperLogMapper.saveSysOperLog(sysOperLog);
    }
}
