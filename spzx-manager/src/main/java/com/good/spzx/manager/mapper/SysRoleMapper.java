package com.good.spzx.manager.mapper;

import com.good.spzx.model.dto.system.SysRoleDto;
import com.good.spzx.model.entity.system.SysRole;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/16 13:41
 */
@Mapper
public interface SysRoleMapper {
    // 查询角色列表
    List<SysRole> findByPage(SysRoleDto sysRoleDto);

    // 添加角色
    void save(SysRole sysRole);

    // 修改角色
    void update(SysRole sysRole);

    // 删除角色
    void deleteById(Integer roleId);

    // 获取所有角色
    List<SysRole> findAll();
}
