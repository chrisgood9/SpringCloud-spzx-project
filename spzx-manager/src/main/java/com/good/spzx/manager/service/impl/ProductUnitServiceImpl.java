package com.good.spzx.manager.service.impl;

import com.good.spzx.manager.mapper.ProductUnitMapper;
import com.good.spzx.manager.service.ProductUnitService;
import com.good.spzx.model.entity.base.ProductUnit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/23 14:33
 */
@Service
public class ProductUnitServiceImpl implements ProductUnitService {

    @Autowired
    private ProductUnitMapper productUnitMapper;

    @Override
    public List<ProductUnit> findAll() {
        return productUnitMapper.findAll();
    }
}
