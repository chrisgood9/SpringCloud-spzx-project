package com.good.spzx.manager.helper;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/18 20:27
 */

import com.good.spzx.model.entity.system.SysMenu;

import java.util.ArrayList;
import java.util.List;

/**
 * 工具类
 * 菜单menu构建树形结构
 */
public class MenuHelper {

    /**
     * 构建菜单
     *
     * @param menuList
     * @return
     */
    public static List<SysMenu> buildTree(List<SysMenu> menuList) {
        // 查询所有的menu，递归获取所有的构建好的节点
        List<SysMenu> treeList = new ArrayList<>();
        for (SysMenu sysMenu : menuList) {
            // 找父节点为0(第一层)的菜单进行 - 递归查找子节点
            if (sysMenu.getParentId().longValue() == 0L) {
                treeList.add(findChildren(sysMenu, menuList));
            }
        }
        return treeList;
    }

    /**
     * 递归查找子节点
     *
     * @param sysMenu
     * @param menuList
     * @return
     */
    private static SysMenu findChildren(SysMenu sysMenu, List<SysMenu> menuList) {
        // 初始化，存储下层数据
        sysMenu.setChildren(new ArrayList<>());
        for (SysMenu it : menuList) {
            // 如果sysMenu的id 是 menuList的parentId => 加入到sysMenu子节点集合中
            if (sysMenu.getId().longValue() == it.getParentId().longValue()) {
                // 继续递归查找，当前子节点的子节点...
                sysMenu.getChildren().add(findChildren(it, menuList));
            }
        }
        return sysMenu;
    }
}
