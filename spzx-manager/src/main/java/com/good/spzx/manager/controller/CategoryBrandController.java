package com.good.spzx.manager.controller;

import com.github.pagehelper.PageInfo;
import com.good.spzx.manager.service.CategoryBrandService;
import com.good.spzx.model.dto.product.CategoryBrandDto;
import com.good.spzx.model.entity.product.Brand;
import com.good.spzx.model.entity.product.CategoryBrand;
import com.good.spzx.model.vo.common.Result;
import com.good.spzx.model.vo.common.ResultCodeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/21 17:47
 */
@RestController
@RequestMapping("/admin/product/categoryBrand")
public class CategoryBrandController {

    @Autowired
    private CategoryBrandService categoryBrandService;

    /**
     * 分页品牌 - 条件分页查询
     *
     * @param current
     * @param limit
     * @param categoryBrandDto
     * @return
     */
    @GetMapping(value = "/getCategoryBrandPage/{current}/{limit}")
    public Result getCategoryBrandPage(@PathVariable("current") Integer current,
                                       @PathVariable("limit") Integer limit,
                                       CategoryBrandDto categoryBrandDto) {
        PageInfo<CategoryBrand> pageInfo = categoryBrandService.getCategoryBrandPage(current, limit, categoryBrandDto);
        return Result.build(pageInfo, ResultCodeEnum.SUCCESS);
    }

    /**
     * 添加
     *
     * @param categoryBrand
     * @return
     */
    @PostMapping(value = "/saveCategoryBrand")
    public Result saveCategoryBrand(@RequestBody CategoryBrand categoryBrand) {
        categoryBrandService.saveCategoryBrand(categoryBrand);
        return Result.build(null, ResultCodeEnum.SUCCESS);
    }

    /**
     * 更新
     *
     * @param categoryBrand
     * @return
     */
    @PutMapping(value = "/updateCategoryBrand")
    public Result updateCategoryBrand(@RequestBody CategoryBrand categoryBrand) {
        categoryBrandService.updateCategoryBrand(categoryBrand);
        return Result.build(null, ResultCodeEnum.SUCCESS);
    }

    /**
     * 根据分类id删除
     *
     * @param id
     * @return
     */
    @DeleteMapping(value = "/deleteCategoryBrand/{id}")
    public Result deleteCategoryBrand(@PathVariable("id") Long id) {
        categoryBrandService.deleteCategoryBrand(id);
        return Result.build(null, ResultCodeEnum.SUCCESS);
    }

    /**
     * 根据分类id查询对应品牌信息
     *
     * @param id
     * @return
     */
    @GetMapping(value = "/findBrandByCategoryId/{id}")
    public Result findBrandByCategoryId(@PathVariable("id") Long id) {
        List<Brand> brand = categoryBrandService.findBrandByCategoryId(id);
        return Result.build(brand, ResultCodeEnum.SUCCESS);
    }

}
