package com.good.spzx.manager.service.impl;

import com.good.spzx.exception.MyException;
import com.good.spzx.manager.helper.MenuHelper;
import com.good.spzx.manager.mapper.SysMenuMapper;
import com.good.spzx.manager.mapper.SysRoleMenuMapper;
import com.good.spzx.manager.service.SysMenuService;
import com.good.spzx.model.entity.system.SysMenu;
import com.good.spzx.model.entity.system.SysUser;
import com.good.spzx.model.vo.common.ResultCodeEnum;
import com.good.spzx.model.vo.system.SysMenuVo;
import com.good.spzx.utils.AuthContextUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/18 20:16
 */
@Service
public class SysMenuServiceImpl implements SysMenuService {

    @Autowired
    private SysMenuMapper sysMenuMapper;

    @Autowired
    private SysRoleMenuMapper sysRoleMenuMapper;

    @Override
    public List<SysMenu> findNodes() {
        List<SysMenu> menuList = sysMenuMapper.selectAll();
        // 构建树形结构
        List<SysMenu> treeList = MenuHelper.buildTree(menuList);
        return treeList;
    }

    /**
     * 新增菜单
     * -- 需要把父菜单- 改成半开状态
     *
     * @param sysMenu
     */
    @Override
    public void saveMenu(SysMenu sysMenu) {
        sysMenuMapper.addMenu(sysMenu);
        // 添加菜单 => 父菜单设置为半开状态（isHalf = 1）
        this.updateSysRoleMenuStatus(sysMenu);
    }

    // 新增子菜单 => 父菜单变成半开状态（isHalf = 1）
    private void updateSysRoleMenuStatus(SysMenu sysMenu) {
        // 获取父菜单
        SysMenu parentMenu = sysMenuMapper.selectParentMenu(sysMenu.getParentId());

        if (parentMenu != null) {
            // 改成半开状态 => isHalf = 1
            sysRoleMenuMapper.updateSysRoleMenuIsHalf(parentMenu.getId());
            // 递归调用，因为有多层结构
            this.updateSysRoleMenuStatus(parentMenu);
        }
    }

    @Override
    public void updateMenu(SysMenu sysMenu) {
        sysMenuMapper.update(sysMenu);
    }

    @Override
    public void deletedById(Long menuId) {
        //如果存在子节点，则不可删除
        Integer count = sysMenuMapper.selectChildren(menuId);
        if (count > 0) {
            throw new MyException(ResultCodeEnum.NODE_ERROR);
        }
        sysMenuMapper.delete(menuId);
    }

    @Override
    public List<SysMenuVo> findUserMenuList() {
        // 从本地线程中获取登录的用户id
        SysUser sysUser = AuthContextUtil.get();
        Long userId = sysUser.getId();
        // 获取当前用户的菜单
        List<SysMenu> menuList = sysMenuMapper.findUserMenuList(userId);
        // 构建树形数据
        List<SysMenu> sysMenusTrees = MenuHelper.buildTree(menuList);
        return this.change(sysMenusTrees);
    }

    // 将 List<SysMenu> 对象转换成 List<SysMenuVo> 对象
    private List<SysMenuVo> change(List<SysMenu> menuList) {
        List<SysMenuVo> menuVosList = new ArrayList<>();

        for (SysMenu it : menuList) {
            SysMenuVo sysMenuVo = new SysMenuVo();
            sysMenuVo.setTitle(it.getTitle());
            sysMenuVo.setName(it.getComponent());
            List<SysMenu> children = it.getChildren();

            if (!children.isEmpty() && children.size() > 0) {
                sysMenuVo.setChildren(change(children));
            }
            menuVosList.add(sysMenuVo);
        }
        return menuVosList;
    }
}
