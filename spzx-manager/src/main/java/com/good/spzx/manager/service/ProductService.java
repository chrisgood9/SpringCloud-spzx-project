package com.good.spzx.manager.service;

import com.github.pagehelper.PageInfo;
import com.good.spzx.model.dto.product.ProductDto;
import com.good.spzx.model.entity.product.Product;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/23 14:52
 */
public interface ProductService {

    // 条件分页查询 - 所有商品信息
    PageInfo<Product> findByPage(Integer page, Integer limit, ProductDto productDto);

    // 保存商品数据
    void saveProduct(Product product);

    // 根据商品id获取商品信息
    Product getProductById(Long id);

    // 根据商品id修改商品信息
    void updateProductById(Product product);

    // 根据商品id删除商品信息
    void deleteById(Long productId);

    // 商品审核
    void updateAuditStatus(Long productId, Integer auditStatus);

    // 商品上下架
    void updateStatus(Long productId, Integer status);
}
