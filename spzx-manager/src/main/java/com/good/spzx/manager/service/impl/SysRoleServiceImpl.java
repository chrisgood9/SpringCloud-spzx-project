package com.good.spzx.manager.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.good.spzx.exception.MyException;
import com.good.spzx.manager.mapper.SysRoleMapper;
import com.good.spzx.manager.mapper.SysUserRoleMapper;
import com.good.spzx.manager.service.SysRoleService;
import com.good.spzx.model.dto.system.SysRoleDto;
import com.good.spzx.model.entity.system.SysRole;
import com.good.spzx.model.vo.common.ResultCodeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/16 13:41
 */
@Service
public class SysRoleServiceImpl implements SysRoleService {

    @Autowired
    private SysRoleMapper sysRoleMapper;

    @Autowired
    private SysUserRoleMapper sysUserRoleMapper;

    // 获取角色列表 —— (条件分页查询)
    @Override
    public PageInfo<SysRole> findByPage(SysRoleDto sysRoleDto, Integer current, Integer limit) {
        // 设置分页参数
        // 本质上是获取数据后，按照传入的参数进行分批的发放（假分页）
        PageHelper.startPage(current, limit);
        // 根据条件查询所有数据
        List<SysRole> sysRoleList = sysRoleMapper.findByPage(sysRoleDto);
        // 封装 pageInfo 对象
        PageInfo<SysRole> pageInfo = new PageInfo<>(sysRoleList);
        return pageInfo;
    }

    // 添加角色
    @Override
    public void saveSysRole(SysRole sysRole) {
        sysRoleMapper.save(sysRole);
    }

    // 修改角色数据
    @Override
    public void updateSysRole(SysRole sysRole) {
        sysRoleMapper.update(sysRole);
    }

    // 删除角色
    @Override
    public void deleteById(Integer roleId) {
        sysRoleMapper.deleteById(roleId);
    }

    // 查询所有角色
    @Override
    public Map<String, Object> findAllRoles(Long userId) {
        Map<String, Object> map = new HashMap<>();
        // 1. 获取所有角色
        List<SysRole> roleList = sysRoleMapper.findAll();
        // 2.获取分配过的角色
        List<Long> roleIds = sysUserRoleMapper.getRoleListByUserId(userId);
        map.put("allRoleList", roleList);
        map.put("roleIds", roleIds);
        return map;
    }
}
