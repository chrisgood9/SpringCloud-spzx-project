package com.good.spzx.manager.controller;

import com.good.spzx.manager.service.FileUploadService;
import com.good.spzx.model.vo.common.Result;
import com.good.spzx.model.vo.common.ResultCodeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/18 10:32
 */
@RestController
@RequestMapping("/admin/system")
public class FileUploadController {

    @Autowired
    private FileUploadService fileUploadService;

    /**
     * 文件上传 —— 头像上传（存储到 Minio）
     * 未指定上传格式 --- 尺寸等
     * @param file
     * @return 返回值：Minio - Url
     */
    @RequestMapping(value = "/fileUpload")
    public Result fileUpload(@RequestParam MultipartFile file) {
        // 返回 Minio 的 url
        String url = fileUploadService.uploadFile(file);
        return Result.build(url, ResultCodeEnum.SUCCESS);
    }
}
