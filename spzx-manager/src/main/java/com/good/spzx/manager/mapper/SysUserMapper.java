package com.good.spzx.manager.mapper;

import com.github.pagehelper.PageInfo;
import com.good.spzx.model.dto.system.SysUserDto;
import com.good.spzx.model.entity.system.SysRole;
import com.good.spzx.model.entity.system.SysUser;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/10/31 16:08
 */
@Mapper
public interface SysUserMapper {

    // 根据用户名查询用户信息
    SysUser selectUserInfoByUserName(String userName);

    // 获取用户信息列表
    List<SysUser> getByPage(SysUserDto sysUserDto);

    // 删除用户
    void deleteById(Integer id);

    // 修改用户
    void update(SysUser sysUser);

    // 添加用户
    void save(SysUser sysUser);
}
