package com.good.spzx.manager.controller;

import com.github.pagehelper.PageInfo;
import com.good.spzx.manager.service.SysRoleService;
import com.good.spzx.model.dto.system.SysRoleDto;
import com.good.spzx.model.entity.system.SysRole;
import com.good.spzx.model.vo.common.Result;
import com.good.spzx.model.vo.common.ResultCodeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/16 13:40
 */
@RestController
@RequestMapping(value = "/admin/system/sysRole")
public class SysRoleController {

    @Autowired
    private SysRoleService sysRoleService;

    /**
     * 获取角色列表
     *
     * @param current    当前页
     * @param limit      每页记录数
     * @param sysRoleDto 条件角色名称对象
     * @return
     */
    @PostMapping(value = "/findByPage/{current}/{limit}")
    public Result findByPage(@PathVariable("current") Integer current,
                             @PathVariable("limit") Integer limit,
                             @RequestBody SysRoleDto sysRoleDto) {
        // PageHelper 插件实现分页查询
        PageInfo<SysRole> pageInfo = sysRoleService.findByPage(sysRoleDto, current, limit);
        return Result.build(pageInfo, ResultCodeEnum.SUCCESS);
    }

    /**
     * 添加角色
     *
     * @param sysRole 前端传入要添加的角色数据
     * @return
     */
    @PostMapping(value = "/saveSysRole")
    public Result saveSysRole(@RequestBody SysRole sysRole) {
        sysRoleService.saveSysRole(sysRole);
        return Result.build(null, ResultCodeEnum.SUCCESS);
    }

    /**
     * 修改角色
     *
     * @param sysRole
     * @return
     */
    @PostMapping(value = "/updateSysRole")
    public Result updateSysRole(@RequestBody SysRole sysRole) {
        sysRoleService.updateSysRole(sysRole);
        return Result.build(null, ResultCodeEnum.SUCCESS);
    }

    /**
     * 删除角色
     *
     * @param roleId
     * @return
     */
    @DeleteMapping(value = "/deleteById/{roleId}")
    public Result deleteById(@PathVariable("roleId") Integer roleId) {
        sysRoleService.deleteById(roleId);
        return Result.build(null, ResultCodeEnum.SUCCESS);
    }

    /**
     * 查询所有角色 和 当前用户id所具有的角色id列表
     *
     * @return
     */
    @GetMapping(value = "/findAllRoles/{userId}")
    public Result findAllRoles(@PathVariable("userId") Long userId) {
        // 返回一个所有角色 和 用户分配过的角色
        Map<String, Object> map = sysRoleService.findAllRoles(userId);
        return Result.build(map, ResultCodeEnum.SUCCESS);
    }
}
