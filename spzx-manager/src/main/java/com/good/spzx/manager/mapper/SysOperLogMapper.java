package com.good.spzx.manager.mapper;

import com.good.spzx.model.entity.system.SysOperLog;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/24 21:42
 */
@Mapper
public interface SysOperLogMapper {

    // 保存日志操作
    void saveSysOperLog(SysOperLog sysOperLog);
}
