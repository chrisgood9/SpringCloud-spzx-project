package com.good.spzx.manager.controller;

import com.good.spzx.manager.service.SysMenuService;
import com.good.spzx.model.entity.system.SysMenu;
import com.good.spzx.model.vo.common.Result;
import com.good.spzx.model.vo.common.ResultCodeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/18 20:15
 */
@RestController
@RequestMapping("/admin/system/sysMenu")
public class SysMenuController {

    @Autowired
    private SysMenuService sysMenuService;

    /**
     * 获取菜单
     *
     * @return
     */
    @GetMapping("/findNodes")
    public Result findNodes() {
        List<SysMenu> sysMenuList = sysMenuService.findNodes();
        return Result.build(sysMenuList, ResultCodeEnum.SUCCESS);
    }

    /**
     * 添加菜单
     *
     * @param sysMenu
     * @return
     */
    @PostMapping(value = "/saveMenu")
    public Result saveMenu(@RequestBody SysMenu sysMenu) {
        sysMenuService.saveMenu(sysMenu);
        return Result.build(null, ResultCodeEnum.SUCCESS);
    }

    /**
     * 修改菜单
     *
     * @param sysMenu
     * @return
     */
    @PutMapping(value = "/updateMenu")
    public Result updateMenu(@RequestBody SysMenu sysMenu) {
        sysMenuService.updateMenu(sysMenu);
        return Result.build(null, ResultCodeEnum.SUCCESS);
    }

    /**
     * 删除菜单
     *
     * @param menuId
     * @return
     */
    @DeleteMapping(value = "/deletedById/{menuId}")
    public Result deletedById(@PathVariable("menuId") Long menuId) {
        sysMenuService.deletedById(menuId);
        return Result.build(null, ResultCodeEnum.SUCCESS);
    }
}
