package com.good.spzx.manager.controller;

import com.github.pagehelper.PageInfo;
import com.good.spzx.manager.service.ProductSpecService;
import com.good.spzx.model.entity.product.ProductSpec;
import com.good.spzx.model.vo.common.Result;
import com.good.spzx.model.vo.common.ResultCodeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Chris
 * @version 1.0
 * @date 2023/11/22 14:21
 */
@RestController
@RequestMapping("/admin/product/productSpec")
public class ProductSpecController {

    @Autowired
    private ProductSpecService productSpecService;

    /**
     * 查询所有商品规格信息 - 条件分页查询
     *
     * @param current
     * @param limit
     * @param productSpec
     * @return
     */
    @GetMapping(value = "/findByPage/{current}/{limit}")
    public Result findByPage(@PathVariable("current") Integer current,
                             @PathVariable("limit") Integer limit,
                             ProductSpec productSpec) {
        PageInfo<ProductSpec> pageInfo = productSpecService.findByPage(current, limit, productSpec);
        return Result.build(pageInfo, ResultCodeEnum.SUCCESS);
    }

    /**
     * 保存商品规格信息
     *
     * @param productSpec
     * @return
     */
    @PostMapping(value = "/saveSpec")
    public Result saveSpec(@RequestBody ProductSpec productSpec) {
        productSpecService.saveSpec(productSpec);
        return Result.build(null, ResultCodeEnum.SUCCESS);
    }

    /**
     * 修改商品规格信息
     *
     * @param productSpec
     * @return
     */
    @PutMapping(value = "/updateSpec")
    public Result updateSpec(@RequestBody ProductSpec productSpec) {
        productSpecService.updateSpec(productSpec);
        return Result.build(null, ResultCodeEnum.SUCCESS);
    }

    /**
     * 根据id删除商品规格信息
     *
     * @param id
     * @return
     */
    @DeleteMapping(value = "/deleteSpec/{id}")
    public Result deleteSpec(@PathVariable("id") Long id) {
        productSpecService.deleteSpec(id);
        return Result.build(null, ResultCodeEnum.SUCCESS);
    }

    /**
     * 查询所有商品规格信息，返回商品规格列表
     *
     * @return
     */
    @GetMapping(value = "/findAll")
    public Result findAll() {
        List<ProductSpec> list = productSpecService.findAll();
        return Result.build(list, ResultCodeEnum.SUCCESS);
    }
}
